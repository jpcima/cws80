
#########
# TOOLS #
#########

find_program(ROS ros)
if(NOT ROS)
  message(FATAL_ERROR "Roswell not found: cannot find the 'ros' command.")
endif()

############
# COMPILER #
############

macro(enable_gcc_warning which)
  if (CMAKE_C_COMPILER_ID MATCHES GNU OR CMAKE_C_COMPILER_ID MATCHES Clang)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -W${which}")
  endif()
  if (CMAKE_CXX_COMPILER_ID MATCHES GNU OR CMAKE_CXX_COMPILER_ID MATCHES Clang)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W${which}")
  endif()
endmacro()

macro(disable_gcc_warning which)
  if (CMAKE_C_COMPILER_ID MATCHES GNU OR CMAKE_C_COMPILER_ID MATCHES Clang)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-${which}")
  endif()
  if (CMAKE_CXX_COMPILER_ID MATCHES GNU OR CMAKE_CXX_COMPILER_ID MATCHES Clang)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-${which}")
  endif()
endmacro()

macro(enable_colors)
  if(CMAKE_GENERATOR STREQUAL Ninja)
    if (CMAKE_C_COMPILER_ID MATCHES GNU OR CMAKE_C_COMPILER_ID MATCHES Clang)
      set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fdiagnostics-color=always")
    endif()
    if (CMAKE_CXX_COMPILER_ID MATCHES GNU OR CMAKE_CXX_COMPILER_ID MATCHES Clang)
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fdiagnostics-color=always")
    endif()
  endif()
endmacro()

macro(enable_assertions type)
  string(TOUPPER "${type}" _type)
  if (CMAKE_C_COMPILER_ID MATCHES GNU OR CMAKE_C_COMPILER_ID MATCHES Clang)
    string(REPLACE "-DNDEBUG" "" CMAKE_C_FLAGS_${_type} "${CMAKE_C_FLAGS_${_type}}")
  endif()
  if (CMAKE_CXX_COMPILER_ID MATCHES GNU OR CMAKE_CXX_COMPILER_ID MATCHES Clang)
    string(REPLACE "-DNDEBUG" "" CMAKE_CXX_FLAGS_${_type} "${CMAKE_CXX_FLAGS_${_type}}")
  endif()
  if (CMAKE_C_COMPILER_ID MATCHES MSVC)
    string(REPLACE "/DNDEBUG" "" CMAKE_C_FLAGS_${_type} "${CMAKE_C_FLAGS_${_type}}")
  endif()
  if (CMAKE_CXX_COMPILER_ID MATCHES MSVC)
    string(REPLACE "/DNDEBUG" "" CMAKE_CXX_FLAGS_${_type} "${CMAKE_CXX_FLAGS_${_type}}")
  endif()
  unset(_type)
endmacro()

#########
# FILES #
#########

macro(create_parent_directories path)
  get_filename_component(_dir "${path}" DIRECTORY)
  file(MAKE_DIRECTORY "${_dir}")
  unset(_dir)
endmacro()

###########
# TARGETS #
###########

macro(add_header_only_library target)
  add_library(${target} INTERFACE)
  set(_includes "${ARGN}")
  foreach(_include IN LISTS _includes)
    include_directories(${target} INTERFACE "${_include}")
  endforeach()
  unset(_includes)
endmacro()

###############
# DYNAMIC API #
###############

macro(add_dynapi_library target output input)
  set(_input_file "${CMAKE_CURRENT_SOURCE_DIR}/${input}")
  set(_output_file "${dynapi_BINARY_DIR}/${output}")
  create_parent_directories("${_output_file}")
  add_custom_command(OUTPUT "${_output_file}"
    COMMAND "${ROS}" -Q "${tools_DIR}/convert-api.ros" "${_input_file}" > "${_output_file}"
    DEPENDS "${tools_DIR}/convert-api.ros" "${_input_file}")
  add_custom_target(${target}--header DEPENDS "${_output_file}")
  add_library(${target} INTERFACE)
  add_dependencies(${target} ${target}--header)
  target_include_directories(${target} INTERFACE "${dynapi_BINARY_DIR}")
  if(CMAKE_SYSTEM_NAME MATCHES Linux)
    target_link_libraries(${target} INTERFACE dl)
  endif()
  unset(_input_file)
  unset(_output_file)
endmacro()

###########
# LINKER #
###########

macro(enable_full_static_link target)
  if(CMAKE_SYSTEM_NAME MATCHES Windows AND
      (CMAKE_C_COMPILER_ID MATCHES GNU OR CMAKE_C_COMPILER_ID MATCHES Clang))
      set_property(TARGET ${target} APPEND_STRING PROPERTY
        LINK_FLAGS " -static-libgcc -static-libstdc++ -Wl,-Bstatic,--whole-archive -lwinpthread -Wl,-Bdynamic,--no-whole-archive")
  endif()
endmacro()

#############
# RESOURCES #
#############

macro(add_resources target)
  set(_list "${ARGN}")
  set(_outputs)
  while(_list)
    list(GET _list 0 _key1)
    list(GET _list 1 _output)
    list(GET _list 2 _key2)
    list(GET _list 3 _input)
    list(REMOVE_AT _list 3 2 1 0)
    if(NOT _key1 STREQUAL OUTPUT)
      message(FATAL_ERROR "invalid syntax")
    endif()
    set(_output "${resources_BINARY_DIR}/${_output}")
    list(APPEND _outputs "${_output}")
    create_parent_directories("${_output}")
    if(_key2 STREQUAL FROM)
      set(_input "${CMAKE_CURRENT_SOURCE_DIR}/${_input}")
    elseif(_key2 STREQUAL TARGET)
      set(_input "$<TARGET_FILE:${_input}>")
    else()
      message(FATAL_ERROR "invalid syntax")
    endif()
    add_custom_command(OUTPUT "${_output}"
      COMMAND "${ROS}" -Q "${tools_DIR}/bin2c.ros" -i "${_input}" -o "${_output}"
      DEPENDS "${tools_DIR}/bin2c.ros" "${_input}")
  endwhile()
  add_custom_target(${target} DEPENDS "${_outputs}")
  unset(_list)
  unset(_length)
  unset(_key1)
  unset(_key2)
  unset(_input_file)
  unset(_output_file)
  unset(_outputs)
endmacro()

################
# LV2 MANIFEST #
################

if(CMAKE_SYSTEM_NAME MATCHES Windows)
  set(lv2_UI_CLASS "ui:WindowsUI")
elseif(CMAKE_SYSTEM_NAME MATCHES Darwin)
  set(lv2_UI_CLASS "ui:CocoaUI")
else()
  set(lv2_UI_CLASS "ui:X11UI")
endif()

macro(add_lv2_manifest target output input)
  set(_input_file "${CMAKE_CURRENT_SOURCE_DIR}/${input}")
  set(_output_file "${CMAKE_CURRENT_BINARY_DIR}/${output}")
  create_parent_directories("${_output_file}")
  add_custom_command(OUTPUT "${_output_file}"
    COMMAND "${ROS}" -Q "${tools_DIR}/configure-file.ros"
            -D "UI_CLASS=${lv2_UI_CLASS}"
            -D "LIB_EXT=${CMAKE_SHARED_LIBRARY_SUFFIX}"
            -i "${_input_file}" -o "${_output_file}"
    DEPENDS "${tools_DIR}/configure-file.ros" "${_input_file}")
  add_custom_target(${target} ALL DEPENDS "${_output_file}")
  unset(_input_file)
  unset(_output_file)
endmacro()

#################
# MACOS BUNDLES #
#################

macro(add_macos_bundle_plist target bundle output)
  set(_input_file "${PROJECT_SOURCE_DIR}/cmake/Info.plist.in")
  set(_output_file "${CMAKE_CURRENT_BINARY_DIR}/${output}")
  set(_args "${ARGN}")
  set(_definitions)
  list(APPEND _definitions "-D")
  list(APPEND _definitions "EXECUTABLE=${_executable}")
  while(_args)
    list(GET _args 0 _key)
    list(GET _args 1 _value)
    list(REMOVE_AT _args 1 0)
    list(APPEND _definitions "-D")
    list(APPEND _definitions "${_key}=${_value}")
  endwhile()
  add_custom_command(OUTPUT "${_output_file}"
    COMMAND "${ROS}" -Q "${tools_DIR}/configure-file.ros"
    "-D" "EXECUTABLE=$<TARGET_FILE_NAME:${bundle}>"
    ${_definitions}
    -i "${_input_file}" -o "${_output_file}"
    DEPENDS "${tools_DIR}/configure-file.ros" "${_input_file}")
  add_custom_target(${target} ALL DEPENDS "${_output_file}")
  unset(_input_file)
  unset(_output_file)
  unset(_args)
  unset(_definitions)
  unset(_key)
  unset(_value)
endmacro()
