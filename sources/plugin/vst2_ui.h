#pragma once
#include "vst2.h"
#include "utility/types.h"
#include <vstgui/vstgui.h>
#include <vstgui/plugin-bindings/aeffguieditor.h>
#include <memory>

namespace cws80 {

class GraphicsDevice;
class InputDevice;

//
class VSTiEditor : public AEffGUIEditor {
 public:
  explicit VSTiEditor(VSTi *effect);
  ~VSTiEditor();

  VSTi *getEffect() override;
  bool getRect(ERect **rect) override;

  bool open(void *ptr) override;
  void close() override;
  void idle() override;
  void draw (ERect *prect) override;

  CMouseEventResult on_mouse_down(CPoint &where, const CButtonState &buttons);
  CMouseEventResult on_mouse_up(CPoint &where, const CButtonState &buttons);
  CMouseEventResult on_mouse_moved(CPoint &where, const CButtonState &buttons);
  bool on_wheel(const CPoint &where, f32 distance, const CButtonState &buttons);
  i32 on_key_down(const VstKeyCode &key);
  i32 on_key_up(const VstKeyCode &key);

  bool do_draw();

  GraphicsDevice &graphics_device() const;
  InputDevice &input_device() const;

 private:
  struct Impl;
  std::unique_ptr<Impl> P;
};

}  // namespace cws80
