#pragma once
#include <lv2/lv2plug.in/ns/lv2core/lv2.h>
#include <lv2/lv2plug.in/ns/ext/options/options.h>
#include <fmt/format.h>
#include <stdexcept>
#include <string.h>

inline const LV2_Feature *lv2_find_feature(
    const LV2_Feature *const features[], const char *uri) {
  for (; features[0]; ++features)
    if (!strcmp(features[0]->URI, uri))
      return features[0];
  return nullptr;
}

inline const LV2_Feature *lv2_require_feature(
    const LV2_Feature *const features[], const char *uri) {
  const LV2_Feature *feature = lv2_find_feature(features, uri);
  if (!feature)
    throw std::logic_error(fmt::format("required feature not found: {}", uri));
  return feature;
}

inline const LV2_Options_Option *lv2_find_option(
    const LV2_Options_Option options[], LV2_URID key) {
  for (; options->key; ++options)
    if (options->key == key)
      return options;
  return nullptr;
}

inline const LV2_Options_Option *lv2_require_option(
    const LV2_Options_Option options[], LV2_URID key,
    const LV2_URID_Unmap *unmap) {
  const LV2_Options_Option *option = lv2_find_option(options, key);
  if (!option) {
    const char *uri = unmap->unmap(unmap->handle, key);
    throw std::logic_error(fmt::format("required option not found: {}", uri));
  }
  return option;
}
