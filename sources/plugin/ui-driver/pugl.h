#pragma once
#include "utility/types.h"
#include <pugl/pugl.h>
#include <memory>
#include <thread>

//------------------------------------------------------------------------------
namespace cws80 {

class UI;
class InputDevice_Pugl;
class GraphicsDevice_GL;
class NativeUI;

//
class UIDriver_Pugl {
 public:
  explicit UIDriver_Pugl(
      UI &ui, InputDevice_Pugl &idev, GraphicsDevice_GL &gdev);
  ~UIDriver_Pugl();

 public:
  bool idle_run();
  void create_widget(void *parent);
  void *widget();

 private:
  UI *ui_ = nullptr;
  InputDevice_Pugl *idev_ = nullptr;
  GraphicsDevice_GL *gdev_ = nullptr;
  std::unique_ptr<NativeUI> native_;
  PuglView *view_ = nullptr;
  std::thread::id ui_thread_id_;
  bool init_ = false;
  bool exposed_ = false;
  bool redraw_needed_ = false;
  static void handle_event(PuglView *view, const PuglEvent *event);
};

}  // namespace cws80
