#include "plugin/ui-driver/pugl.h"
#include "ui/ui_helpers_native.h"
#include "ui/device/dev_input_pugl.h"
#include "ui/device/dev_graphics_gl.h"
#include "cws80_ui.h"
#include "utility/scope_guard.h"
#include <fmt/format.h>
#include <stdexcept>
#include <stdio.h>

namespace cws80 {

UIDriver_Pugl::UIDriver_Pugl(
    UI &ui, InputDevice_Pugl &idev, GraphicsDevice_GL &gdev)
    : ui_(&ui), idev_(&idev), gdev_(&gdev) {
}

UIDriver_Pugl::~UIDriver_Pugl() {
  PuglView *view = view_;
  if (!view)
    return;

  if (std::this_thread::get_id() != ui_thread_id_) {
    fprintf(stderr, "error: caught cleanup attempt from outside of UI thread\n");
    return;
  }

  puglDestroy(view);
}

bool UIDriver_Pugl::idle_run() {
  PuglView *view = view_;
  if (!view)
    return false;

  puglProcessEvents(view);
  InputDevice_Pugl &idev = *idev_;
  idev.flush_events();

  puglPostRedisplay(view);
  return exposed_;
}

void UIDriver_Pugl::create_widget(void *parent) {
  bool success = false;

  if (view_)
    throw std::logic_error("view already initialized");

  ui_thread_id_ = std::this_thread::get_id();

  int pugl_argc = 1;
  char pugl_arg0[] = "pugl";
  char *pugl_argv[] = {pugl_arg0, nullptr};

  UI *ui = ui_;
  PuglView *view = puglInit(&pugl_argc, pugl_argv);
  if (!view)
    throw std::runtime_error("error creating a Pugl view");
  view_ = view;

  SCOPE(exit) {
    if (!success) { puglDestroy(view); view_ = nullptr; } };

  puglSetHandle(view, this);
  puglSetEventFunc(view, &handle_event);

  puglInitWindowParent(view, (PuglNativeWindow)parent);
  puglInitWindowSize(view, ui->width(), ui->height());
  puglInitResizable(view, false);
  puglInitContextType(view, PUGL_GL);

  if (puglCreateWindow(view, ui->title()) != 0)
    throw std::runtime_error("error creating a Pugl window");
  puglShowWindow(view);

  success = true;
}

void *UIDriver_Pugl::widget() {
  PuglView *view = view_;
  return (void *)puglGetNativeWindow(view);
}

void UIDriver_Pugl::handle_event(PuglView *view, const PuglEvent *event) {
  auto *self = (UIDriver_Pugl *)puglGetHandle(view);
  UI &ui = *self->ui_;
  PuglEventType evtype = event->type;

  if (!self->init_) {
    if (evtype == PUGL_EXPOSE) {
      puglEnterContext(view);
      void *window = (void *)(uintptr_t)puglGetNativeWindow(view);
      NativeUI *nat = NativeUI::create(window);
      self->native_.reset(nat);
      ui.initialize(*self->gdev_, *nat, window);
      puglLeaveContext(view, false);
      puglPostRedisplay(view);
      self->init_ = true;
    }
    return;
  }

  InputDevice_Pugl &idev = *self->idev_;
  bool interact = idev.on_event(*event);

  switch (evtype) {
    case PUGL_EXPOSE: {
      puglEnterContext(view);
      bool updates = ui.draw();
      bool redraw = updates || self->redraw_needed_;
      puglLeaveContext(view, redraw);
      if (redraw)
        self->redraw_needed_ = false;
      self->exposed_ = true;
      break;
    }

    case PUGL_CLOSE:
      self->exposed_ = false; break;

    case PUGL_VISIBILITY:
      if (event->visibility.visible) {
        self->redraw_needed_ = true;
        break;
      }

    case PUGL_CONFIGURE:
      break;

    default:
      break;
  }

  if (interact && self->exposed_)
    puglPostRedisplay(view);
}

}  // namespace cws80
