#include "cws80_ui.h"
#include "cws80_messages.h"
#include "plugin/ui-driver/pugl.h"
#include "plugin/plug_ui_master.h"
#include "plugin/lv2_util.h"
#include "ui/device/dev_input_pugl.h"
#include "ui/device/dev_graphics_gl.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include "utility/types.h"
#include <lv2/lv2plug.in/ns/lv2core/lv2.h>
#include <lv2/lv2plug.in/ns/ext/atom/atom.h>
#include <lv2/lv2plug.in/ns/ext/atom/forge.h>
#include <lv2/lv2plug.in/ns/extensions/ui/ui.h>
#include <fmt/format.h>

namespace cws80 {

static const char ui_uri[] = "urn:jpcima:cws80#ui";

class LV2UIMaster;

struct Instance {
  std::unique_ptr<LV2UIMaster> master;
  std::unique_ptr<InputDevice_Pugl> idev;
  std::unique_ptr<GraphicsDevice_GL> gdev;
  std::unique_ptr<UI> ui;
  std::unique_ptr<UIDriver_Pugl> driver;
  struct {
    LV2_URID event_transfer;
    LV2_URID notification;
    LV2_URID request;
  } uris;
  LV2UI_Write_Function write = nullptr;
  LV2UI_Controller controller = nullptr;
  LV2_Atom_Forge forge;
  LV2_URID_Map *map = nullptr;
  LV2_URID_Unmap *unmap = nullptr;
};

class LV2UIMaster : public UIMaster {
 public:
  explicit LV2UIMaster(Instance &instance) : instance_(instance) {}
  void emit_request(const Request::T &req) override;
  void set_parameter_automated(uint idx, i32 val) override;
  void begin_edit(uint idx) override;
  void end_edit(uint idx) override;
 private:
  Instance &instance_;
};

void LV2UIMaster::emit_request(const Request::T &req) {
  Instance &instance = instance_;
  LV2_Atom_Forge *forge = &instance.forge;
  auto uris = instance.uris;

  size_t reqsize = RequestTraits(req.type).size();
  size_t bufsize = reqsize + 32;
  dynarray<u8> buf(bufsize);
  lv2_atom_forge_set_buffer(forge, buf.data(), bufsize);

  LV2_Atom_Forge_Ref ref = lv2_atom_forge_atom(forge, reqsize, uris.request);
  if (!ref || !lv2_atom_forge_raw(forge, &req, reqsize)) {
    assert(false);
    return;
  }
  lv2_atom_forge_pad(forge, reqsize);

  LV2_Atom *atom = lv2_atom_forge_deref(forge, ref);
  size_t atomsize = lv2_atom_total_size(atom);
  instance.write(
      instance.controller, 2,  // event port
      atomsize, uris.event_transfer, atom);
}

void LV2UIMaster::set_parameter_automated(uint idx, i32 val) {
  debug("Set parameter automated {} value {}", idx, val);

  i32 min, max;
  std::tie(min, max) = Program::get_parameter_range(idx);
  Request::SetParameter req;
  req.index = idx;
  req.value = clamp(val, min, max);
  emit_request(req);
}

void LV2UIMaster::begin_edit(uint idx) {
  debug("Begin editing {}", idx);
}

void LV2UIMaster::end_edit(uint idx) {
  debug("End editing {}", idx);
}

static LV2UI_Handle instantiate(
    const LV2UI_Descriptor *descriptor,
    const char *plugin_uri,
    const char *bundle_path,
    LV2UI_Write_Function write_function,
    LV2UI_Controller controller,
    LV2UI_Widget *widget,
    const LV2_Feature *const *features) {
  debug("In LV2UI::instantiate");

  std::unique_ptr<Instance> instance;

  const LV2_Feature
      *ft_urid_map = lv2_require_feature(features, LV2_URID__map),
      *ft_urid_unmap = lv2_require_feature(features, LV2_URID__unmap),
      *ft_ui_resize = lv2_find_feature(features, LV2_UI__resize),
      *ft_ui_parent = lv2_find_feature(features, LV2_UI__parent) ;

  auto *map = (LV2_URID_Map *)ft_urid_map->data;
  auto *unmap = (LV2_URID_Unmap *)ft_urid_unmap->data;
  LV2UI_Resize *resize = nullptr;
  void *parent = nullptr;
  if (ft_ui_resize)
    resize = (LV2UI_Resize *)ft_ui_resize->data;
  if (ft_ui_parent)
    parent = ft_ui_parent->data;

  try {
    debug("Creating UI instance");
    instance.reset(new Instance);
    instance->master.reset(new LV2UIMaster(*instance));

    UI *ui = new UI(*instance->master);
    instance->ui.reset(ui);
    InputDevice_Pugl *idev = new InputDevice_Pugl(*ui);
    instance->idev.reset(idev);
    GraphicsDevice_GL *gdev = new GraphicsDevice_GL(*ui);
    instance->gdev.reset(gdev);
    UIDriver_Pugl *driver = new UIDriver_Pugl(*ui, *idev, *gdev);
    instance->driver.reset(driver);

    debug("Creating window");
    driver->create_widget(parent);
    *widget = driver->widget();

    if (resize)
      resize->ui_resize(resize->handle, ui->width(), ui->height());

    instance->uris.event_transfer = map->map(map->handle, LV2_ATOM__eventTransfer);
    instance->uris.notification = map->map(map->handle, CWS80__Notification);
    instance->uris.request = map->map(map->handle, CWS80__Request);
    instance->write = write_function;
    instance->controller = controller;
    lv2_atom_forge_init(&instance->forge, map);
    instance->map = map;
    instance->unmap = unmap;
  } catch (std::exception &ex) {
    debug_exception(ex);
    *widget = nullptr;
    instance.reset();
  }

  return instance.release();
}

static void cleanup(LV2UI_Handle handle) {
  debug("In LV2UI::cleanup");

  auto *instance = (Instance *)handle;
  delete instance;
}

static void port_event(
    LV2UI_Handle handle,
    u32 port_index, u32 buffer_size,
    u32 format, const void *buffer) {
  auto *instance = (Instance *)handle;
  UI *ui = instance->ui.get();
  auto uris = instance->uris;

  if (port_index == 3 && format == uris.event_transfer) {  // notification
    auto *atom = (const LV2_Atom *)buffer;
    if (atom->type == uris.notification) {
      u32 size = atom->size;
      auto *body = (const Notification::T *)LV2_ATOM_BODY_CONST(atom);
      if (size >= sizeof(Notification::T) &&
          size >= NotificationTraits(body->type).size())
        ui->receive_notification(*body);
    }
  }
}

static int idle(LV2UI_Handle handle) {
  auto *instance = (Instance *)handle;
  UIDriver_Pugl *driver = instance->driver.get();
  return driver->idle_run();
}

static const void *extension_data(const char *uri) {
  if (!strcmp(uri, LV2_UI__idleInterface)) {
    static constexpr LV2UI_Idle_Interface intf = { &idle };
    return &intf;
  }
  return nullptr;
}

static const LV2UI_Descriptor descriptor = {
  ui_uri,
  &instantiate,
  &cleanup,
  &port_event,
  &extension_data,
};

LV2_SYMBOL_EXPORT
const LV2UI_Descriptor *lv2ui_descriptor(u32 index) {
  return (index == 0) ? &descriptor: nullptr;
}

}  // namespace cws80
