#pragma once
#include "utility/ringbuffer.h"
#include "utility/types.h"
#include <audioeffectx.h>
#include <memory>

namespace cws80 {

class FxMaster;
class Instrument;

//
class VSTi : public AudioEffectX {
 public:
  explicit VSTi(audioMasterCallback master);
  ~VSTi();

  void open() override;
  void close() override;

  i32 canDo(char *feature) override;
  i32 getNumMidiInputChannels() override;
  i32 getNumMidiOutputChannels() override;

  void setBlockSize(i32 bs) override;

  i32 processEvents(VstEvents *events) override;
  void processReplacing(f32 *inputs[], f32 *outputs[], i32 nframes) override;

  f32 getParameter(i32 index) override;
  void setParameter(i32 index, f32 value) override;
  i32 getProgram() override;
  void setProgram(i32 pgm) override;
  void setProgramName(char *name) override;
  void getProgramName(char *name) override;
  i32 getChunk(void **data, bool isPreset) override;
  i32 setChunk(void *data, i32 size, bool isPreset) override;

  ringbuffer &notification_buffer();
  ringbuffer &request_buffer();
  const Instrument &instrument() const;

 private:
  struct Impl;
  std::unique_ptr<Impl> P;
};

}  // namespace cws80
