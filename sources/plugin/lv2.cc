#include "cws80_ins.h"
#include "cws80_messages.h"
#include "plugin/plug_fx_master.h"
#include "plugin/lv2_util.h"
#include "utility/debug.h"
#include "utility/types.h"
#include <lv2/lv2plug.in/ns/lv2core/lv2.h>
#include <lv2/lv2plug.in/ns/ext/urid/urid.h>
#include <lv2/lv2plug.in/ns/ext/options/options.h>
#include <lv2/lv2plug.in/ns/ext/buf-size/buf-size.h>
#include <lv2/lv2plug.in/ns/ext/atom/atom.h>
#include <lv2/lv2plug.in/ns/ext/atom/forge.h>
#include <lv2/lv2plug.in/ns/ext/atom/util.h>
#include <lv2/lv2plug.in/ns/ext/midi/midi.h>
#include <fmt/format.h>
#include <memory>
#include <stdio.h>

namespace cws80 {

static const char plugin_uri[] = "urn:jpcima:cws80";

class LV2FxMaster;

struct Instance {
  std::unique_ptr<LV2FxMaster> master;
  std::unique_ptr<Instrument> ins;
  f32 *outl = nullptr, *outr = nullptr;
  LV2_Atom_Sequence *events = nullptr;
  LV2_Atom_Sequence *notifs = nullptr;
  u32 maxbuflen = 0;
  std::unique_ptr<i16[]> outbufs;
  LV2_Atom_Forge forge;
  LV2_Atom_Forge_Frame notifs_frame;
  struct {
    LV2_URID midi_event;
    LV2_URID notification;
    LV2_URID request;
  } uris;
};

class LV2FxMaster : public FxMaster {
 public:
  explicit LV2FxMaster(Instance &instance) : instance_(instance) {}
  void emit_notification(const Notification::T &ntf) override;
 private:
  Instance &instance_;
};

void LV2FxMaster::emit_notification(const Notification::T &ntf) {
  Instance &instance = instance_;
  auto uris = instance.uris;
  LV2_Atom_Forge *forge = &instance.forge;
  u32 size = NotificationTraits(ntf.type).size();
  lv2_atom_forge_frame_time(forge, 0);
  lv2_atom_forge_atom(forge, size, uris.notification);
  lv2_atom_forge_raw(forge, &ntf, size);
  lv2_atom_forge_pad(forge, size);
}

static LV2_Handle instantiate(
    const LV2_Descriptor *descriptor,
    f64 sample_rate,
    const char *bundle_path,
    const LV2_Feature *const features[]) {
  std::unique_ptr<Instance> instance;
  debug("In LV2::instantiate");

  try {
    const LV2_Feature
        *ft_urid_map = lv2_require_feature(features, LV2_URID__map),
        *ft_urid_unmap = lv2_require_feature(features, LV2_URID__unmap),
        *ft_options = lv2_require_feature(features, LV2_OPTIONS__options);

    auto *map = (LV2_URID_Map *)ft_urid_map->data;
    auto *unmap = (LV2_URID_Unmap *)ft_urid_unmap->data;
    auto *options = (LV2_Options_Option *)ft_options->data;

    const LV2_Options_Option
        *opt_max_block_length = lv2_require_option(
            options, map->map(map->handle, LV2_BUF_SIZE__maxBlockLength), unmap);

    u32 maxbuflen = *(u32 *)opt_max_block_length->value;
    debug("Buffer size {}", maxbuflen);

    debug("Creating effect instance");
    instance.reset(new Instance);
    instance->master.reset(new LV2FxMaster(*instance));
    instance->ins.reset(new Instrument(*instance->master));
    instance->maxbuflen = maxbuflen;
    instance->outbufs.reset(new i16[2 * maxbuflen]);

    debug("Initializing instrument");
    instance->ins->initialize(sample_rate, maxbuflen);

    lv2_atom_forge_init(&instance->forge, map);
    instance->uris.midi_event = map->map(map->handle, LV2_MIDI__MidiEvent);
    instance->uris.notification = map->map(map->handle, CWS80__Notification);
    instance->uris.request = map->map(map->handle, CWS80__Request);
  } catch (std::exception &ex) {
    debug_exception(ex);
    instance.reset();
  }

  return instance.release();
}

static void connect_port(LV2_Handle handle, u32 port, void *data) {
  auto *instance = (Instance *)handle;
  switch (port) {
    case 0: instance->outl = (f32 *)data; break;
    case 1: instance->outr = (f32 *)data; break;
    case 2: instance->events = (LV2_Atom_Sequence *)data; break;
    case 3: instance->notifs = (LV2_Atom_Sequence *)data; break;
  }
}

static void activate(LV2_Handle handle) {
  debug("In LV2::activate");
}

static void run(LV2_Handle handle, u32 nframes) {
  auto *instance = (Instance *)handle;
  Instrument &ins = *instance->ins;
  LV2_Atom_Forge *forge = &instance->forge;
  LV2_Atom_Forge_Frame *notifs_frame = &instance->notifs_frame;
  auto uris = instance->uris;

  lv2_atom_forge_set_buffer(
      forge, (u8 *)instance->notifs, instance->notifs->atom.size);
  lv2_atom_forge_sequence_head(forge, notifs_frame, 0);

  // midi events
  const LV2_Atom_Sequence *seq = instance->events;
  LV2_ATOM_SEQUENCE_FOREACH(seq, /* LV2_Atom_Event * */event) {
    const u32 type = event->body.type;
    const u32 size = event->body.size;
    auto *body = (const u8 *)LV2_ATOM_BODY_CONST(&event->body);
    if (type == uris.midi_event) {
      ins.receive_midi(body, size, (uint)event->time.frames);
    } else if (type == uris.request) {
      auto &req = *(const Request::T *)body;
      if (size >= sizeof(Request::T) && size >= RequestTraits(req.type).size())
        ins.receive_request(req);
    }
  }

  // audio frames
  i16 *outl = instance->outbufs.get();
  i16 *outr = outl + instance->maxbuflen;
  ins.synthesize(outl, outr, nframes);

  f32 *portl = instance->outl;
  f32 *portr = instance->outr;
  for (uint i = 0; i < nframes; ++i) {
    portl[i] = outl[i] / 32767.0f;
    portr[i] = outr[i] / 32767.0f;
  }

  lv2_atom_forge_pop(forge, notifs_frame);
}

static void deactivate(LV2_Handle handle) {
  debug("In LV2::deactivate");

  auto *instance = (Instance *)handle;

  Instrument &ins = *instance->ins;
  ins.reset();
}

static void cleanup(LV2_Handle handle) {
  debug("In LV2::cleanup");

  auto *instance = (Instance *)handle;
  delete instance;
}

static const void *extension_data(const char *uri) {
  return nullptr;
}

static LV2_Descriptor descriptor = {
  plugin_uri,
  &instantiate,
  &connect_port,
  &activate,
  &run,
  &deactivate,
  &cleanup,
  &extension_data,
};

}  // namespace cws80

LV2_SYMBOL_EXPORT
const LV2_Descriptor *lv2_descriptor(u32 index) {
  return (index == 0) ? &cws80::descriptor : nullptr;
}
