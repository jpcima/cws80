#include <winsock2.h>
#include <windows.h>

BOOL WINAPI DllMain(
    HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
  if (fdwReason == DLL_PROCESS_ATTACH) {
    WSADATA wsadata;
    if (WSAStartup(MAKEWORD(2, 2), &wsadata) != 0)
      return FALSE;
  } else if (fdwReason == DLL_PROCESS_DETACH) {
    WSACleanup();
  }
  return TRUE;
}
