#include "plugin/vst2.h"
#include "plugin/vst2_ui.h"
#include "plugin/plug_fx_master.h"
#include "cws80_ins.h"
#include "utility/arithmetic.h"
#include "utility/types.h"
#include "utility/debug.h"
#include <fmt/format.h>
#include <chrono>
#include <memory>
#include <string.h>
namespace stc = std::chrono;

namespace cws80 {

static const uint num_programs = Bank::max_programs;
static const uint num_params = Param::num_params;
// TODO unique identifier
static const uint unique_id = CCONST('c', 'w', 's', '8');

//------------------------------------------------------------------------------
class VSTFxMaster : public FxMaster {
 public:
  explicit VSTFxMaster(VSTi *fx) : fx_(fx) {}
  void emit_notification(const Notification::T &ntf) override;
 private:
  VSTi *fx_ = nullptr;
};

void VSTFxMaster::emit_notification(const Notification::T &ntf) {
  VSTi *fx = fx_;
  ringbuffer &rb = fx->notification_buffer();
  rb.put(&ntf, NotificationTraits(ntf.type).size());
}

//------------------------------------------------------------------------------
struct VSTi::Impl {
  std::unique_ptr<FxMaster> master_;
  std::unique_ptr<Instrument> ins_;
  std::unique_ptr<i16[]> outbufs_;
  static constexpr uint ringbuffer_capacity = 512 * 1024;
  ringbuffer rb_ntf_{ringbuffer_capacity};
  ringbuffer rb_req_{ringbuffer_capacity};
  u32 bufsize_ = 0;
  bool has_started_processing_ = false;
};

//------------------------------------------------------------------------------
VSTi::VSTi(audioMasterCallback master)
    : AudioEffectX(master, num_programs, num_params),
      P(new Impl) {
  setUniqueID(unique_id);  // XXX unique identifier
  isSynth();
  setNumInputs(0);
  setNumOutputs(2);
  canProcessReplacing();
  programsAreChunks();
  setEditor(new VSTiEditor(this));
}

VSTi::~VSTi() {
}

void VSTi::open() {
  debug("In VSTi::open");

  AudioEffectX::open();

  P->master_.reset(new VSTFxMaster(this));
  P->ins_.reset(new Instrument(*P->master_));

  setBlockSize(getBlockSize());

  P->has_started_processing_ = false;
}

void VSTi::close() {
  debug("In VSTi::close");
  AudioEffectX::close();
}

i32 VSTi::canDo(char *feature_) {
  if (!strcmp(feature_, "receiveVstEvents"))
    return 1;
  if (!strcmp(feature_, "receiveVstMidiEvents"))
    return 1;
  // if (!strcmp(feature_, "midiProgramNames"))
  //   return 1;
  return -1;
}

i32 VSTi::getNumMidiInputChannels() {
  return 1;
}

i32 VSTi::getNumMidiOutputChannels() {
  return 0;
}

void VSTi::setBlockSize(i32 bs) {
  AudioEffectX::setBlockSize(bs);

  if (P->has_started_processing_)
    return;  // ignore and keep current block size

  if ((u32)bs == P->bufsize_)
    return;

  debug("Buffer size {}", bs);
  P->outbufs_.reset(new i16[2 * bs]);

  debug("Initializing instrument");
  P->ins_->initialize(getSampleRate(), bs);

  P->bufsize_ = bs;
}

i32 VSTi::processEvents(VstEvents *events) {
  Instrument &ins = *P->ins_;
  for (u32 i = 0, n = events->numEvents; i < n; ++i) {
    const VstEvent *evgeneric = events->events[i];
    switch (evgeneric->type) {
      case kVstMidiType: {
        auto *ev = (const VstMidiEvent *)evgeneric;
        auto *msg = (const u8 *)ev->midiData;
        u32 ftime = ev->deltaFrames;
        ins.receive_midi(msg, 3, ftime);
        break;
      }
      case kVstSysExType: {
        auto *ev = (const VstMidiSysexEvent *)evgeneric;
        auto *msg = (const u8 *)ev->sysexDump;
        u32 ftime = ev->deltaFrames;
        ins.receive_sysex(msg, ev->dumpBytes, ftime);
        break;
      }
    }
  }

  return 1;
}

void VSTi::processReplacing(f32 *inputs[], f32 *outputs[], i32 nframes) {
  P->has_started_processing_ = true;

  Instrument &ins = *P->ins_;
  u32 bufsize = P->bufsize_;

  //
  ringbuffer &rb = request_buffer();
  for (RequestType reqtype; rb.peek(&reqtype, sizeof(RequestType));) {
    u8 buf[RequestTraits::max_size()];
    bool getok = rb.get(buf, RequestTraits(reqtype).size());
    (void)getok;
    assert(getok);
    ins.receive_request(*(Request::T *)buf);
  }

  f32 *portl = outputs[0];
  f32 *portr = outputs[1];
  i16 *outl = P->outbufs_.get();
  i16 *outr = outl + bufsize;

  while ((u32)nframes > 0) {
    u32 currframes = std::min(bufsize, (u32)nframes);
    ins.synthesize(outl, outr, currframes);
    for (u32 i = 0; i < currframes; ++i) {
      portl[i] = outl[i] / 32767.0f;
      portr[i] = outr[i] / 32767.0f;
    }
    portl += currframes;
    portr += currframes;
    nframes -= currframes;
  }
}

f32 VSTi::getParameter(i32 index) {
  Instrument &ins = *P->ins_;
  i32 min, max;
  std::tie(min, max) = Program::get_parameter_range(index);
  i32 val = clamp(ins.get_parameter(index), min, max);
  return (f32)(val - min) / (max - min);
}

void VSTi::setParameter(i32 index, f32 value) {
  Instrument &ins = *P->ins_;
  ins.set_f32_parameter(index, value);
}

i32 VSTi::getProgram() {
  Instrument &ins = *P->ins_;
  return ins.program_number();
}

void VSTi::setProgram(i32 pgm) {
  Instrument &ins = *P->ins_;
  ins.select_program(ins.bank_number(), pgm);
}

void VSTi::setProgramName(char *name) {
  Instrument &ins = *P->ins_;
  ins.rename_program(name);
}

void VSTi::getProgramName(char *name) {
  Instrument &ins = *P->ins_;
  ins.program_name(name);
}

i32 VSTi::getChunk(void **data, bool isPreset) {
  Instrument &ins = *P->ins_;
  if (isPreset) {
    const Program &pgm = ins.active_program();
    *data = (void *)&pgm;
    return sizeof(pgm);
  } else {
    const std::array<Bank, 4> &banks = ins.banks();
    *data = (void *)banks.data();
    return banks.size() * sizeof(banks[0]);
  }
}

i32 VSTi::setChunk(void *data, i32 size, bool isPreset) {
  Instrument &ins = *P->ins_;
  size_t count;
  if (isPreset) {
    Program pgm {};
    count = sizeof(pgm);
    memcpy(&pgm, data, std::min<size_t>(size, count));
    ins.enable_program(pgm);
  } else {
    std::array<Bank, 4> banks {};
    count = banks.size() * sizeof(banks[0]);
    memcpy(banks.data(), data, std::min<size_t>(size, count));
    for (uint i = 0; i < banks.size(); ++i)
      ins.load_bank(i, banks[i]);
    ins.enable_program(ins.selected_program());
  }
  return count;
}

ringbuffer &VSTi::notification_buffer() {
  return P->rb_ntf_;
}

ringbuffer &VSTi::request_buffer() {
  return P->rb_req_;
}

const Instrument &VSTi::instrument() const {
  return *P->ins_;
}

}  // namespace cws80

//------------------------------------------------------------------------------
AudioEffect *createEffectInstance(audioMasterCallback master) {
  try {
    return new cws80::VSTi(master);
  } catch (std::exception &ex) {
    debug_exception(ex);
    return nullptr;
  }
}

extern "C" {

#if defined(_WIN32)
# define VST_EXPORT __declspec(dllexport)
#elif defined(__GNUC__)
# define VST_EXPORT [[gnu::visibility("default")]]
#endif

VST_EXPORT AEffect *VSTPluginMain(audioMasterCallback master) {
  debug("In VSTPluginMain");

  if (!master(0, audioMasterVersion, 0, 0, 0, 0))
    return nullptr;  // old version
  AudioEffect *effect = createEffectInstance(master);
  return effect ? effect->getAeffect() : nullptr;
}

#if defined(__APPLE__)
VST_EXPORT AEffect *main_macho(audioMasterCallback master) { return VSTPluginMain(master); }
#elif defined(_WIN32)
VST_EXPORT AEffect *MAIN(audioMasterCallback master) { return VSTPluginMain(master); }
#elif defined(__BEOS__)
VST_EXPORT AEffect *main_plugin(audioMasterCallback master) { return VSTPluginMain(master); }
#endif

} // extern "C"
