#include "plugin/vst2_ui.h"
#include "plugin/plug_ui_master.h"
#include "ui/ui_helpers_vst.h"
#include "ui/device/dev_input_vst.h"
#include "ui/device/dev_graphics_gl.h"
#include "ui/device/dev_graphics_gdip.h"
#include "utility/debug.h"
#include "cws80_ins.h"
#include "cws80_ui.h"
#include <vstgui/lib/platform/iplatformframe.h>
#include <fmt/format.h>
#include <boost/intrusive_ptr.hpp>
#include <chrono>
namespace stc = std::chrono;

namespace VSTGUI {
inline void intrusive_ptr_add_ref(IReference *ref) { ref->remember(); }
inline void intrusive_ptr_release(IReference *ref) { ref->forget(); }
}

namespace cws80 {

class VSTUIMaster : public UIMaster {
 public:
  explicit VSTUIMaster(VSTiEditor *ed) : ed_(ed) {}
  VSTi *effect() const { return ed_->getEffect(); }
  void emit_request(const Request::T &req) override;
  void set_parameter_automated(uint idx, i32 val) override;
  void begin_edit(uint idx) override;
  void end_edit(uint idx) override;
 private:
  VSTiEditor *ed_ = nullptr;
};

void VSTUIMaster::emit_request(const Request::T &req) {
  VSTi *fx = effect();
  ringbuffer &rb = fx->request_buffer();
  rb.put(&req, RequestTraits(req.type).size());
}

void VSTUIMaster::set_parameter_automated(uint idx, i32 val) {
  VSTi *fx = ed_->getEffect();
  const Instrument &ins = fx->instrument();
  i32 min, max;
  std::tie(min, max) = Program::get_parameter_range(idx);
  val = clamp(val, min, max);
  if (val != ins.get_parameter(idx)) {
    debug("Set parameter automated {} value {}", idx, val);
    fx->setParameterAutomated(idx, (f32)(val - min) / (max - min));
  }
}

void VSTUIMaster::begin_edit(uint idx) {
  VSTi *fx = ed_->getEffect();
  debug("Begin editing {}", idx);
  fx->beginEdit(idx);
}

void VSTUIMaster::end_edit(uint idx) {
  VSTi *fx = ed_->getEffect();
  debug("End editing {}", idx);
  fx->endEdit(idx);
}

//------------------------------------------------------------------------------
struct VSTiEditor::Impl {
  std::unique_ptr<VSTUIMaster> master_;
  std::unique_ptr<InputDevice_Vst> idev_;
  std::unique_ptr<GraphicsDevice> gdev_;
  std::unique_ptr<NativeUI_Vst> native_;
  std::unique_ptr<UI> ui_;
  bool ui_init_ = false;
  CView *view_ = nullptr;
  stc::steady_clock::time_point tdraw_ = stc::steady_clock::now();
  ERect rect_ {};
  CPoint mousepos_ {};
  CButtonState mousebtn_ {};
  bool entered_draw_ = false;
};

//------------------------------------------------------------------------------
VSTiEditor::VSTiEditor(VSTi *effect)
    : AEffGUIEditor(effect),
      P(new Impl) {
  debug("Creating editor");

  P->rect_.right = UI::width();
  P->rect_.bottom = UI::height();
}

VSTiEditor::~VSTiEditor() {
}

VSTi *VSTiEditor::getEffect() {
  return static_cast<VSTi *>(AEffEditor::getEffect());
}

bool VSTiEditor::getRect(ERect **rect) {
  *rect = &P->rect_;
  return true;
}

//------------------------------------------------------------------------------
class VSTiOpenGLView : public COpenGLView {
 public:
  VSTiOpenGLView(const CRect &size, VSTiEditor &ed);
  void platformOpenGLViewCreated() override;
  void platformOpenGLViewWillDestroy() override;
  void drawOpenGL(const CRect &) override;

  CMouseEventResult onMouseDown(CPoint &where, const CButtonState &buttons) override;
  CMouseEventResult onMouseUp(CPoint &where, const CButtonState &buttons) override;
  CMouseEventResult onMouseMoved(CPoint &where, const CButtonState &buttons) override;
  bool onWheel(const CPoint &where, const f32 &distance, const CButtonState &buttons) override;
  i32 onKeyDown(VstKeyCode &key) override;
  i32 onKeyUp(VstKeyCode &key) override;

  IPlatformOpenGLView *getPlatformOpenGLView() const;

 private:
  VSTiEditor &ed_;
};

//------------------------------------------------------------------------------
class VSTiGraphicsView : public CView {
 public:
  VSTiGraphicsView(const CRect &size, VSTiEditor &ed);
  void draw(CDrawContext *pdc) override;

  CMouseEventResult onMouseDown(CPoint &where, const CButtonState &buttons) override;
  CMouseEventResult onMouseUp(CPoint &where, const CButtonState &buttons) override;
  CMouseEventResult onMouseMoved(CPoint &where, const CButtonState &buttons) override;
  bool onWheel(const CPoint &where, const f32 &distance, const CButtonState &buttons) override;
  i32 onKeyDown(VstKeyCode &key) override;
  i32 onKeyUp(VstKeyCode &key) override;

 private:
  VSTiEditor &ed_;
};

//------------------------------------------------------------------------------
bool VSTiEditor::open(void *ptr) {
  debug("In VSTiEditor::open");

  AEffGUIEditor::open(ptr);

  bool success = false;
  std::unique_ptr<VSTUIMaster> master;
  std::unique_ptr<UI> ui;
  CFrame *frame = nullptr;
  bool frame_is_open = false;

  SCOPE(exit) {
    if (!success && frame) {
      if (frame_is_open) frame->close(); else frame->forget();
      this->frame = nullptr;
    }
  };

  try {
    debug("Creating frame");
    const ERect &rect = P->rect_;
    const CRect framerect(rect.left, rect.top, rect.right, rect.bottom);
    frame = this->frame = new CFrame(framerect, this);

    debug("Opening frame with parent {}", ptr);
    frame_is_open = frame->open(ptr);
    if (!frame_is_open)
      throw std::runtime_error("cannot open frame");

    NativeUI_Vst *nat = new NativeUI_Vst(frame);
    P->native_.reset(nat);

    IPlatformFrame *pfframe = frame->getPlatformFrame();

    master.reset(new VSTUIMaster(this));
    ui.reset(new UI(*master));
    P->idev_.reset(new InputDevice_Vst(*ui));

    boost::intrusive_ptr<CView> view;
    if (false) {
#if defined(_WIN32)
      HWND hwnd = (HWND)pfframe->getPlatformRepresentation();
      P->gdev_.reset(new GraphicsDevice_Gdip(*ui, hwnd));
      view.reset(new VSTiGraphicsView(framerect, *this), false);
#else
      throw std::runtime_error("software rendering not implemented on this platform");
#endif
    } else {
      P->gdev_.reset(new GraphicsDevice_GL(*ui));
      debug("Creating openGL view");
      view.reset(new VSTiOpenGLView(framerect, *this));
    }
    P->view_ = view.get();
    frame->addView(view.get());
  } catch (std::exception &ex) {
    debug_exception(ex);
    return false;
  }

  P->ui_ = std::move(ui);
  P->master_ = std::move(master);
  success = true;
  return true;
}

void VSTiEditor::close() {
  debug("In VSTiEditor::close");

  GraphicsDevice &gdev = *P->gdev_;
  IPlatformOpenGLView *pfv = nullptr;

  if (gdev.type() == GraphicsType::OpenGL) {
    CView *view = P->view_;
    VSTiOpenGLView *glview = static_cast<VSTiOpenGLView *>(view);
    pfv = glview->getPlatformOpenGLView();
  }

  {
    if (pfv) pfv->lockContext();
    SCOPE(exit) { if (pfv) pfv->unlockContext(); };

    if (pfv) pfv->makeContextCurrent();

    P->ui_.reset();
    P->ui_init_ = false;
    P->gdev_.reset();
    P->idev_.reset();
    P->master_.reset();
  }

  P->view_ = nullptr;
  this->frame->close();
  this->frame = nullptr;

  AEffGUIEditor::close();
}

bool VSTiEditor::do_draw() {
  // debug("About to draw");

  assert(!P->entered_draw_);
  if (P->entered_draw_)
    return false;

  P->entered_draw_ = true;
  SCOPE(exit) { P->entered_draw_ = false; };

  UI *ui = P->ui_.get();
  InputDevice_Vst &idev = *P->idev_;
  GraphicsDevice &gdev = *P->gdev_;
  NativeUI_Vst &nat = *P->native_;

  if (!P->ui_init_) {
    debug("Initializing user interface");
    CFrame *frame = this->frame;
    IPlatformFrame *pfframe = frame->getPlatformFrame();
    ui->initialize(gdev, nat, pfframe->getPlatformRepresentation());
    P->ui_init_ = true;
  }

  P->tdraw_ = stc::steady_clock::now();
  idev.flush_events();
  return ui->draw();
}

GraphicsDevice &VSTiEditor::graphics_device() const {
  return *P->gdev_;
}

InputDevice &VSTiEditor::input_device() const {
  return *P->idev_;
}

void VSTiEditor::idle() {
  UI *ui = P->ui_.get();
  VSTi *fx = getEffect();

  //
  ringbuffer &rb = fx->notification_buffer();
  for (NotificationType ntftype; rb.peek(&ntftype, sizeof(NotificationType));) {
    u8 buf[NotificationTraits::max_size()];
    bool getok = rb.get(buf, NotificationTraits(ntftype).size());
    (void)getok;
    assert(getok);
    ui->receive_notification(*(Notification::T *)buf);
  }

  if (isOpen()) {
    stc::steady_clock::time_point now = stc::steady_clock::now();
    if (now - P->tdraw_ >= stc::milliseconds(30))
      P->view_->invalid();
  }

  AEffGUIEditor::idle();
}

void VSTiEditor::draw(ERect *prect) {
  AEffGUIEditor::draw(prect);
}

CMouseEventResult VSTiEditor::on_mouse_down(
    CPoint &pos, const CButtonState &btn) {
  CButtonState &oldstate = P->mousebtn_;
  CButtonState state = oldstate | btn;

  if ((state & kLButton) && !(oldstate & kLButton))
    P->idev_->on_mouse_down(1, (int)pos.x, (int)pos.y);
  if ((state & kMButton) && !(oldstate & kMButton))
    P->idev_->on_mouse_down(3, (int)pos.x, (int)pos.y);
  if ((state & kRButton) && !(oldstate & kRButton))
    P->idev_->on_mouse_down(2, (int)pos.x, (int)pos.y);

  oldstate = state;
  return kMouseEventHandled;
}

CMouseEventResult VSTiEditor::on_mouse_up(
    CPoint &pos, const CButtonState &btn) {
  CButtonState &oldstate = P->mousebtn_;
  CButtonState state = oldstate & ~btn;

  if (!(state & kLButton) && (oldstate & kLButton))
    P->idev_->on_mouse_up(1, (int)pos.x, (int)pos.y);
  if (!(state & kMButton) && (oldstate & kMButton))
    P->idev_->on_mouse_up(3, (int)pos.x, (int)pos.y);
  if (!(state & kRButton) && (oldstate & kRButton))
    P->idev_->on_mouse_up(2, (int)pos.x, (int)pos.y);

  oldstate = state;
  return kMouseEventHandled;
}

CMouseEventResult VSTiEditor::on_mouse_moved(
    CPoint &pos, const CButtonState &btn) {
  CPoint &oldpos = P->mousepos_;
  if (oldpos != pos) {
    P->idev_->on_mouse_move((int)pos.x, (int)pos.y);
    oldpos = pos;
  }
  return kMouseEventHandled;
}

bool VSTiEditor::on_wheel(
    const CPoint &where, f32 distance, const CButtonState &buttons) {
  const f32 wheel_delta = 120;
  P->idev_->on_mouse_wheel((int)(distance * wheel_delta), (int)where.x, (int)where.y);
  return true;
}

i32 VSTiEditor::on_key_down(const VstKeyCode &key) {
  P->idev_->on_key_down(key);
  return 1;
}

i32 VSTiEditor::on_key_up(const VstKeyCode &key) {
  P->idev_->on_key_up(key);
  return 1;
}

//------------------------------------------------------------------------------
VSTiOpenGLView::VSTiOpenGLView(const CRect &size, VSTiEditor &ed)
    : COpenGLView(size), ed_(ed) {
}

void VSTiOpenGLView::platformOpenGLViewCreated() {
  debug("Created openGL context");
}

void VSTiOpenGLView::platformOpenGLViewWillDestroy() {
  debug("About to destroy openGL context");

  IPlatformOpenGLView *pfview = getPlatformOpenGLView();
  pfview->lockContext();
  SCOPE(exit) { pfview->unlockContext(); };

  pfview->makeContextCurrent();
  ed_.graphics_device().setup_context();
}

void VSTiOpenGLView::drawOpenGL(const CRect &) {
  IPlatformOpenGLView *pfv = getPlatformOpenGLView();
  if (ed_.do_draw())
    pfv->swapBuffers();
}

CMouseEventResult VSTiOpenGLView::onMouseDown(
    CPoint &where, const CButtonState &buttons) {
  return ed_.on_mouse_down(where, buttons);
}

CMouseEventResult VSTiOpenGLView::onMouseUp(
    CPoint &where, const CButtonState &buttons) {
  return ed_.on_mouse_up(where, buttons);
}

CMouseEventResult VSTiOpenGLView::onMouseMoved(
    CPoint &where, const CButtonState &buttons) {
  return ed_.on_mouse_moved(where, buttons);
}

bool VSTiOpenGLView::onWheel(
    const CPoint &where, const f32 &distance, const CButtonState &buttons) {
  return ed_.on_wheel(where, distance, buttons);
}

i32 VSTiOpenGLView::onKeyDown(VstKeyCode &key) {
  return ed_.on_key_down(key);
}

i32 VSTiOpenGLView::onKeyUp(VstKeyCode &key) {
  return ed_.on_key_up(key);
}

IPlatformOpenGLView *VSTiOpenGLView::getPlatformOpenGLView() const {
  return COpenGLView::getPlatformOpenGLView();
}

//------------------------------------------------------------------------------
VSTiGraphicsView::VSTiGraphicsView(const CRect &size, VSTiEditor &ed)
    : CView(size), ed_(ed) {
}

void VSTiGraphicsView::draw(CDrawContext *pdc) {
  CView::draw(pdc);
  ed_.do_draw();
}

CMouseEventResult VSTiGraphicsView::onMouseDown(
    CPoint &where, const CButtonState &buttons) {
  return ed_.on_mouse_down(where, buttons);
}

CMouseEventResult VSTiGraphicsView::onMouseUp(
    CPoint &where, const CButtonState &buttons) {
  return ed_.on_mouse_up(where, buttons);
}

CMouseEventResult VSTiGraphicsView::onMouseMoved(
    CPoint &where, const CButtonState &buttons) {
  return ed_.on_mouse_moved(where, buttons);
}

bool VSTiGraphicsView::onWheel(
    const CPoint &where, const f32 &distance, const CButtonState &buttons) {
  return ed_.on_wheel(where, distance, buttons);
}

i32 VSTiGraphicsView::onKeyDown(VstKeyCode &key) {
  return ed_.on_key_down(key);
}

i32 VSTiGraphicsView::onKeyUp(VstKeyCode &key) {
  return ed_.on_key_up(key);
}

}  // namespace cws80
