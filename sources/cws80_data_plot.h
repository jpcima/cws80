#pragma once
#include "cws80_data.h"
#include "utility/types.h"

namespace cws80 {

void plot_waves(const Wave waves[], const char *titles[], uint count);
void plot_waveset(Waveset waveset);

}  // namespace cws80
