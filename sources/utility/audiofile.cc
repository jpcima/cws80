#include "audiofile.h"
#include <boost/endian/buffers.hpp>
#include <boost/integer.hpp>

namespace audiofile_detail {

template <class Data> struct au_format_traits;

template <> struct au_format_traits<i16> {
  static constexpr u32 encoding = 3;
};
template <> struct au_format_traits<f32> {
  static constexpr u32 encoding = 6;
};

struct AuHeader {
  boost::endian::big_uint32_buf_t magic;
  boost::endian::big_uint32_buf_t data_offset;
  boost::endian::big_uint32_buf_t data_size;
  boost::endian::big_uint32_buf_t encoding;
  boost::endian::big_uint32_buf_t sample_rate;
  boost::endian::big_uint32_buf_t channel_count;
};

}  // namespace au_file_detail

template <class Data>
void AudioFileOutput<Data>::start(uint sample_rate, uint channel_count) {
  file_.truncate();
  audiofile_detail::AuHeader hdr;
  hdr.magic = 0x2e736e64;
  hdr.data_offset = sizeof(hdr);
  hdr.data_size = UINT32_MAX;
  hdr.encoding = audiofile_detail::au_format_traits<Data>::encoding;
  hdr.sample_rate = sample_rate;
  hdr.channel_count = channel_count;
  file_.write(&hdr, sizeof(hdr));
}

template <class Data>
void AudioFileOutput<Data>::finish() {
  StdioFile::offset_type length = file_.tell();
  file_.seek(offsetof(audiofile_detail::AuHeader, data_size), SEEK_SET);
  length -= sizeof(audiofile_detail::AuHeader);
  if (length > UINT32_MAX)
    throw std::range_error("audio file too large");
  boost::endian::big_uint32_buf_t data_size((u32)length);
  file_.write(&data_size, sizeof(data_size));
  file_.flush();
}

template <class Data>
void AudioFileOutput<Data>::put(Data x) {
  typedef typename boost::uint_t<8 * sizeof(x)>::exact uint_type;
  union { Data d; uint_type n; } u { x };
  uint_type big = boost::endian::native_to_big(u.n);
  file_.write(&big, sizeof(big));
}

template <class Data>
void AudioFileOutput<Data>::write(const Data *x, uint n) {
  for (uint i = 0; i < n; ++i)
    put(x[i]);
}

template class AudioFileOutput<i16>;
template class AudioFileOutput<f32>;
