#pragma once
#include "utility/types.h"
#include <c++std/optional>
#include <limits>
#include <type_traits>
#include <cassert>

template <class T>
struct opt_integer_traits {
  static constexpr T null_value =
      std::is_signed<T>() ?
      std::numeric_limits<T>::min(): std::numeric_limits<T>::max();
};

//
template <class T, T NullValue = opt_integer_traits<T>::null_value>
class opt_integer {
 public:
  constexpr opt_integer() noexcept {}
  constexpr opt_integer(T x) noexcept : x_(x) {}
  template <class U> constexpr opt_integer(opt_integer<U> y) noexcept
    : x_((y.x_ != y.nval) ? y.x_ : nval) {}

  constexpr explicit operator bool() const noexcept { return x_ != nval; }
  constexpr bool has_value() const noexcept { return x_ != nval; }
  constexpr void reset(T x = nval) noexcept  { x_ = x; }

  constexpr T operator*() const noexcept { assert(x_ != nval); return x_; }
  constexpr T value() const { if (x_ == nval) throw cxx::bad_optional_access(); return x_; }
  constexpr T value_or(T y) const noexcept { return (x_ != nval) ? x_ : y; }

  constexpr bool operator==(T y) const noexcept { return x_ != nval && x_ == y; }
  constexpr bool operator!=(T y) const noexcept { return !(*this == y); }

  template <class U> constexpr bool operator==(opt_integer<U> y) const noexcept
    { return (x_ != nval) ? (y.x_ != y.nval && x_ == y.x_) : (y.x_ == y.nval); }
  template <class U> constexpr bool operator!=(opt_integer<U> y) const noexcept
    { return !(*this == y); }

  constexpr opt_integer(const opt_integer &) noexcept = default;
  constexpr opt_integer(opt_integer &&) noexcept = default;

  constexpr opt_integer &operator=(const opt_integer &) noexcept = default;
  constexpr opt_integer &operator=(opt_integer &&) noexcept = default;

 private:
  static constexpr T nval = NullValue;
  T x_ = nval;
};

typedef std::make_signed_t<size_t> ssize_t;
typedef opt_integer<size_t> opt_size_t;
typedef opt_integer<ssize_t> opt_ssize_t;

typedef opt_integer<short> opt_short;
typedef opt_integer<ushort> opt_ushort;
typedef opt_integer<int> opt_int;
typedef opt_integer<uint> opt_uint;
typedef opt_integer<long> opt_long;
typedef opt_integer<ulong> opt_ulong;
typedef opt_integer<longlong> opt_longlong;
typedef opt_integer<ulonglong> opt_ulonglong;

typedef opt_integer<i8> opt_i8;
typedef opt_integer<i16> opt_i16;
typedef opt_integer<i32> opt_i32;
typedef opt_integer<i64> opt_i64;
typedef opt_integer<u8> opt_u8;
typedef opt_integer<u16> opt_u16;
typedef opt_integer<u32> opt_u32;
typedef opt_integer<u64> opt_u64;
