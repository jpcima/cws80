#include "utility/debug.h"
#if defined(_WIN32)
# include <spdlog/sinks/wincolor_sink.h>
# include <spdlog/sinks/msvc_sink.h>
#else
# include <spdlog/sinks/ansicolor_sink.h>
# include <spdlog/sinks/syslog_sink.h>
#endif
#include <c++std/string_view>
#include <mutex>
#include <memory>

namespace debug_detail {

#if !defined(NDEBUG)

static std::mutex logger_mutex;
static std::shared_ptr<spdlog::logger> the_debug_logger;

spdlog::logger &debug_logger() {
  std::lock_guard<std::mutex> lock(logger_mutex);
  std::shared_ptr<spdlog::logger> logger = the_debug_logger;
  if (!logger) {
    spdlog::sink_ptr sinks[] = {
#if defined(_WIN32)
      spdlog::sink_ptr(new spdlog::sinks::wincolor_stderr_sink_mt),
      spdlog::sink_ptr(new spdlog::sinks::msvc_sink_mt),
#else
      spdlog::sink_ptr(new spdlog::sinks::ansicolor_stderr_sink_mt),
      spdlog::sink_ptr(new spdlog::sinks::syslog_sink(PROJECT_IDENTIFIER)),
#endif
    };
    uint nsinks = sizeof(sinks) / sizeof(sinks[0]);
    logger = spdlog::create(PROJECT_IDENTIFIER, sinks + 0, sinks + nsinks);
    logger->set_level(spdlog::level::debug);
    logger->set_pattern("(T:%t)%v");
    the_debug_logger = logger;
  }
  return *logger;
}

#endif

}  // namespace debug_detail
