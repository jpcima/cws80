#pragma once
#include "utility/types.h"
#include <fmt/format.h>
#include <spdlog/spdlog.h>
#if defined(_WIN32)
# include <windows.h>
#elif defined(__APPLE__)
# include <os/log.h>
#endif
#include <boost/core/demangle.hpp>
#include <exception>
#include <stdio.h>

#define DEBUG_INCLUDE_SOURCE_LOCATION 1
#define DEBUG_INCLUDE_THREAD_ID 1
#define DEBUG_SOURCE_LOCATION_FORMAT "({}:{})"

#if defined(NDEBUG)

#define debug(...)
#define debug_exception(...)

#else

#define debug(fmt, ...)                         \
  ::debug_detail::debug_logger().debug(         \
       DEBUG_SOURCE_LOCATION_FORMAT fmt,        \
       DEBUG_FILE_NAME, __LINE__,               \
       ##__VA_ARGS__)
#define debug_exception(ex)                                    \
  do {                                                         \
    const std::exception &ex__##__LINE__ = (ex);               \
    const char *msg__##__LINE__ = ex__##__LINE__.what();       \
    msg__##__LINE__ = msg__##__LINE__ ?                        \
                      msg__##__LINE__ : "<no message>";        \
    (::debug_detail::debug_logger().debug)(                    \
        DEBUG_SOURCE_LOCATION_FORMAT "exception {}: {}",       \
        DEBUG_FILE_NAME, __LINE__,                             \
        boost::core::demangle(typeid(ex__##__LINE__).name()),  \
        msg__##__LINE__);                                      \
  } while (0)

namespace debug_detail {

spdlog::logger &debug_logger();
template <size_t N> constexpr auto comptime_basename(const char (&c)[N]);
#define DEBUG_FILE_NAME ((const char *)::debug_detail::comptime_basename(__FILE__).data)

}  // namespace debug_detail

//------------------------------------------------------------------------------
namespace debug_detail {

template <size_t N>
inline constexpr auto comptime_basename(const char (&c)[N]) {
  const size_t n = N - 1;
  size_t i = n;
#if defined(_WIN32)
  const char os_sep = '\\';
#else
  const char os_sep = '/';
#endif
  while (i > 0 && c[i-1] != '/' && c[i-1] != os_sep)
    --i;
  size_t nb = (i > 0) ? (n - i) : n;
  struct ct_string_buf { char data[N]; } b {};
  for (size_t i = 0; i < nb; ++i)
    b.data[i] = c[n - nb + i];
  return b;
}

}  // namespace debug_detail

#endif
