#include "utility/ringbuffer.h"
#include <algorithm>

// initialization and cleanup

ringbuffer::ringbuffer(u32 capacity)
    : cap_(capacity) {
  rbdata_.reset(new u8[capacity]);
}

ringbuffer::~ringbuffer() {
}

// read operations

u32 ringbuffer::used_byte_count() const {
  const u32 rp = rp_, wp = wp_, cap = cap_;
  return wp + ((wp < rp) ? cap : 0) - rp;
}

inline bool ringbuffer::read_impl_(void *data, u32 len, bool advp) {
  if (used_byte_count() < len)
    return false;
  const u32 rp = rp_, cap = cap_;
  u8 *dst = (u8 *)data;
  if (data) {
    const uint taillen = std::min(len, cap - rp);
    std::copy_n(&rbdata_[rp], taillen, dst);
    std::copy_n(&rbdata_[0], len - taillen, dst + taillen);
  }
  if (advp)
    rp_ = (rp + len < cap) ? (rp + len) : (rp + len - cap);
  return true;
}

bool ringbuffer::get(void *data, u32 len) {
  return read_impl_(data, len, true);
}

bool ringbuffer::peek(void *data, u32 len) const {
  return const_cast<ringbuffer *>(this)->read_impl_(data, len, false);
}

bool ringbuffer::discard(u32 len) {
  return read_impl_(nullptr, len, true);
}

// write operations

u32 ringbuffer::free_byte_count() const {
  const u32 rp = rp_, wp = wp_, cap = cap_;
  return rp + ((rp <= wp) ? cap : 0) - wp - 1;
}

bool ringbuffer::put(const void *data, u32 len) {
  if (free_byte_count() < len)
    return false;
  const u32 wp = wp_, cap = cap_;
  const u8 *src = (const u8 *)data;
  const uint taillen = std::min(len, cap - wp);
  std::copy_n(src, taillen, &rbdata_[wp]);
  std::copy_n(src + taillen, len - taillen, &rbdata_[0]);
  wp_ = (wp + len < cap) ? (wp + len) : (wp + len - cap);
  return true;
}
