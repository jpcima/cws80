#include "stdfile.h"
#include "scope_guard.h"
#include <system_error>
#include <errno.h>
#include <fcntl.h>
#if defined(_WIN32)
# include <io.h>
# include <direct.h>
#else
# include <unistd.h>
#endif

#ifdef P_tmpdir
#include <random>
static std::mt19937_64 prng64(std::random_device{}());
#endif

StdioFile::StdioFile()
    : handle_(nullptr, &::fclose) {
}

StdioFile::StdioFile(const char *path, const char *mode)
    : handle_(nullptr, &::fclose) {
  FILE *handle = ::fopen(path, mode);
  if (!handle)
    throw std::system_error(errno, std::generic_category());
  handle_.reset(handle);
}

StdioFile::StdioFile(int fd, const char *mode, bool owned)
    : handle_(nullptr, &::fclose) {
  if (owned) {
#if defined(_WIN32)
    fd = ::_dup(fd);
#else
    fd = ::dup(fd);
#endif
    if (fd == -1)
      throw std::system_error(errno, std::generic_category());
  }

#if defined(_WIN32)
  SCOPE(exit) { if (owned && fd != -1) ::_close(fd); };
#else
  SCOPE(exit) { if (owned && fd != -1) ::close(fd); };
#endif

#if defined(_WIN32)
  FILE *handle = ::_fdopen(fd, mode);
#else
  FILE *handle = ::fdopen(fd, mode);
#endif
  if (!handle)
    throw std::system_error(errno, std::generic_category());
  fd = -1;
  handle_.reset(handle);
}

StdioFile::StdioFile(const char *path, int oflag, int omode)
    : handle_(nullptr, &::fclose) {
#if defined(_WIN32)
  int fd = ::_open(path, oflag, omode);
#else
  int fd = ::open(path, oflag, omode);
#endif
  if (fd == -1)
    throw std::system_error(errno, std::generic_category());

#if defined(_WIN32)
  SCOPE(exit) { if (fd != -1) ::_close(fd); };
#else
  SCOPE(exit) { if (fd != -1) ::close(fd); };
#endif

  char mode[16];
  uint modei = 0;
  if (oflag & O_RDONLY) {
    mode[modei++] = 'r';
  } else if (oflag & O_WRONLY) {
    if (oflag & O_APPEND) {
      mode[modei++] = 'a';
    } else {
      mode[modei++] = 'w';
    }
  } else if (oflag & O_RDWR) {
    if (oflag & O_APPEND) {
      mode[modei++] = 'a';
      mode[modei++] = '+';
    } else if (oflag & O_CREAT) {
      mode[modei++] = 'w';
      mode[modei++] = '+';
    } else {
      mode[modei++] = 'r';
      mode[modei++] = '+';
    }
  }
  mode[modei++] = 'b';
  mode[modei++] = '\0';

#if defined(_WIN32)
  FILE *handle = ::_fdopen(fd, mode);
#else
  FILE *handle = ::fdopen(fd, mode);
#endif
  if (!handle)
    throw std::system_error(errno, std::generic_category());
  fd = -1;
  handle_.reset(handle);
}

void StdioFile::close() {
  if (handle_.get_deleter()(handle_.release()) == -1)
    throw std::system_error(errno, std::generic_category());
}

auto StdioFile::read(void *ptr, size_type size) -> size_type {
  size_type count = ::fread(ptr, 1, size, handle_.get());
  if (count != size) {
    int err = errno;
    if (::ferror(handle_.get()))
      throw std::system_error(err, std::generic_category());
  }
  return count;
}

void StdioFile::write(const void *ptr, size_type size) {
  size_type count = ::fwrite(ptr, 1, size, handle_.get());
  if (count != size)
    throw std::system_error(errno, std::generic_category());
}

int StdioFile::getc() {
  int ret = ::fgetc(handle_.get());
  if (ret == -1) {
    int err = errno;
    if (::ferror(handle_.get()))
      throw std::system_error(err, std::generic_category());
  }
  return ret;
}

void StdioFile::putc(char c) {
  if (::fputc(c, handle_.get()) == -1)
    throw std::system_error(errno, std::generic_category());
}

void StdioFile::puts(cxx::string_view c) {
  write(c.data(), c.size());
}

bool StdioFile::getline(std::string &linebuf, size_type linemax) {
  bool endl = false;
  linebuf.clear();
  while (!endl && linebuf.size() < linemax) {
    int c = getc();
    if (c == -1)
      return !linebuf.empty();
    linebuf.push_back(c);
    endl = c == '\n';
  }
  return true;
}

void StdioFile::flush() {
  if (::fflush(handle_.get()) == -1)
    throw std::system_error(errno, std::generic_category());
}

auto StdioFile::tell() -> offset_type {
#if defined(_WIN32)
  offset_type off = ::_ftelli64(handle_.get());
#else
  offset_type off = ::ftello(handle_.get());
#endif
  if (off == offset_type(-1))
    throw std::system_error(errno, std::generic_category());
  return off;
}

void StdioFile::seek(offset_type offset, int whence) {
#if defined(_WIN32)
  if (::_fseeki64(handle_.get(), offset, whence) == -1)
#else
  if (::fseeko(handle_.get(), offset, whence) == -1)
#endif
    throw std::system_error(errno, std::generic_category());
}

void StdioFile::truncate(offset_type size) {
  flush();
  seek(0, SEEK_SET);
#ifdef _WIN32
  if (::_chsize_s(::_fileno(handle_.get()), size) != 0)
#else
  if (::ftruncate(::fileno(handle_.get()), size) == -1)
#endif
    throw std::system_error(errno, std::generic_category());
}

auto StdioFile::length() -> offset_type {
  offset_type off = tell();
  seek(0, SEEK_END);
  offset_type size = tell();
  seek(off, SEEK_SET);
  return size;
}

uint StdioFile::printf(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  uint n = vprintf(fmt, ap);
  va_end(ap);
  return n;
}

uint StdioFile::vprintf(const char *fmt, va_list ap) {
  errno = 0;
  uint n = ::vfprintf(handle_.get(), fmt, ap);
  int err = errno;
  if (err != 0)
    throw std::system_error(err, std::generic_category());
  return n;
}

uint StdioFile::scanf(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  uint n = ::scanf(fmt, ap);
  va_end(ap);
  return n;
}

uint StdioFile::vscanf(const char *fmt, va_list ap) {
  errno = 0;
  uint n = ::vfscanf(handle_.get(), fmt, ap);
  int err = errno;
  if (err != 0)
    throw std::system_error(err, std::generic_category());
  return n;
}

StdioFile::operator bool() const {
  return bool(handle_);
}

FILE *StdioFile::get() const {
  return handle_.get();
}

FILE *StdioFile::release() {
  return handle_.release();
}

int StdioFile::fd() const {
#if defined(_WIN32)
  return ::_fileno(handle_.get());
#else
  return ::fileno(handle_.get());
#endif
}

auto StdioFile::null() -> StdioFile {
#if defined(_WIN32)
  const char *path = "nul";
#else
  const char *path = "/dev/null";
#endif
  return StdioFile(path, "wb");
}

auto StdioFile::temporary() -> StdioFile {
  StdioFile file;
  FILE *handle = ::tmpfile();
  if (!handle)
    throw std::system_error(errno, std::generic_category());
  file.handle_.reset(handle);
  return file;
}

auto StdioFile::named_temporary(std::string &path) -> StdioFile {
  while (true) {
#ifdef P_tmpdir
    char pathbuf[] = P_tmpdir "/XXXXXXXX";
    size_t pathbuflen = sizeof(pathbuf) - 1;
    u64 pathrnd = prng64();
    for (uint i = 0; i < 8; ++i) {
      static const char al[] =
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      pathbuf[pathbuflen-1-i] = al[(pathrnd >> 8 * i) % (sizeof(al)-1)];
    }
#else
#if defined(_WIN32)
    char *pathbuf = _tempnam(nullptr, nullptr);
#else
    char *pathbuf = tempnam(nullptr, nullptr);
#endif
    if (!pathbuf)
      throw std::system_error(errno, std::generic_category());
    SCOPE(exit)  { ::free(pathbuf); };
#endif

#if defined(_WIN32)
    int fd = ::_open(pathbuf, O_CREAT|O_EXCL|O_RDWR, 0600);
#else
    int fd = ::open(pathbuf, O_CREAT|O_EXCL|O_RDWR, 0600);
#endif
    if (fd == -1 && errno != EEXIST)
      throw std::system_error(errno, std::generic_category());

#if defined(_WIN32)
    SCOPE(exit)  { if (fd != -1) ::_close(fd); };
#else
    SCOPE(exit)  { if (fd != -1) ::close(fd); };
#endif

    if (fd != -1) {
      StdioFile file(fd, "w+b", true);
      fd = -1;
      path.assign(pathbuf);
      path.c_str();
      return file;
    }
  }
}

auto StdioFile::command(const char *command, const char *mode) -> StdioFile {
  StdioFile file;
#if defined(_WIN32)
  FILE *handle = ::_popen(command, mode);
#else
  FILE *handle = ::popen(command, mode);
#endif
  if (!handle)
    throw std::system_error(errno, std::generic_category());
#if defined(_WIN32)
  file.handle_ = std::unique_ptr<FILE, int(*)(FILE *)>(handle, &::_pclose);
#else
  file.handle_ = std::unique_ptr<FILE, int(*)(FILE *)>(handle, &::pclose);
#endif
  return file;
}

auto StdioFile::command(const char *argv[], const char *mode) -> StdioFile {
  std::string cmd;
  cmd.reserve(1024);
  for (const char *arg; (arg = *argv); ++argv) {
    if (!cmd.empty())
      cmd.push_back(' ');
#if defined(_WIN32)
    cmd.push_back('"');
    for (char c;; ++arg) {
      uint numbackslash = 0;
      while ((c = *arg) == '\\') {
        ++c;
        ++numbackslash;
      }
      if (!c) {
        cmd.append(numbackslash * 2, '\\');
        break;
      }
      if (c == '"')
        cmd.append(numbackslash * 2 + 1, '\\');
      else
        cmd.append(numbackslash, '\\');
      cmd.push_back(c);
    }
    cmd.push_back('"');
#else
    for (char c; (c = *arg); ++arg) {
      if (c == '\n') {
        cmd.append("'\n'");
     } else {
        bool alpha = (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        bool num = (c >= '0' && c <= '9');
        if (!alpha && !num)
          cmd.push_back('\\');
        cmd.push_back(c);
      }
    }
#endif
  }
  return command(cmd.c_str(), mode);
}

bool StdioFile::remove_file(const char *path) {
#if defined(_WIN32)
  return ::_unlink(path) == 0;
#else
  return ::unlink(path) == 0;
#endif
}

bool StdioFile::remove_directory(const char *path) {
#if defined(_WIN32)
  return ::_rmdir(path) == 0;
#else
  return ::rmdir(path) == 0;
#endif
}
