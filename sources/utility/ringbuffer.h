#pragma once
#include "utility/types.h"
#include <memory>

class ringbuffer final {
 public:
  // initialization and cleanup
  explicit ringbuffer(u32 capacity);
  ~ringbuffer();

  // read operations
  u32 used_byte_count() const;
  bool get(void *data, u32 len);
  bool peek(void *data, u32 len) const;
  bool discard(u32 len);

  // write operations
  u32 free_byte_count() const;
  bool put(const void *data, u32 len);

 private:
  const u32 cap_;
  volatile u32 rp_ = 0, wp_ = 0;
  std::unique_ptr<u8[]> rbdata_ {};
  bool read_impl_(void *data, u32 len, bool advp);
};
