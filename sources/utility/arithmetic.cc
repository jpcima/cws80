#include "utility/arithmetic.h"

f64 itp_lagrange(f64 x, f64 x0, f64 dx, gsl::span<const f64> ys) {
  const f64 *y = ys.data();
  const uint k = ys.size();
  if (k == 0)
    return 0;
  f64 r = 0;
  for (uint j = 0; j < k; ++j) {
    f64 l = 1;
    f64 xj = x0 + dx * j;
#pragma omp simd reduction(*:l)
    for (uint m = 0; m < k; ++m) {
      f64 xm = x0 + dx * m;
      l *= (m == j) ? 1.0 : ((x - xm) / (xj - xm));
    }
    r += y[j] * l;
  }
  return r;
}
