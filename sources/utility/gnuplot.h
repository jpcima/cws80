#pragma once
#include <c++std/optional>
#include <c++std/string_view>
#include <string>
#include "utility/stdfile.h"
#include "utility/types.h"

StdioFile gp_open();
std::string gp_escape(cxx::string_view str);

void gp_xrange(StdioFile &gp, cxx::optional<f64> x1, cxx::optional<f64> x2);
void gp_yrange(StdioFile &gp, cxx::optional<f64> y1, cxx::optional<f64> y2);
void gp_y2range(StdioFile &gp, cxx::optional<f64> y1, cxx::optional<f64> y2);

void gp_pause(StdioFile &gp);
