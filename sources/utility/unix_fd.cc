#include "unix_fd.h"
#include <system_error>
#include <fcntl.h>
#ifdef _WIN32
# include <io.h>
#else
# include <unistd.h>
#endif

void unix_fd::reset(int fd) noexcept {
  if (fd == fd_) return;
#ifdef _WIN32
  if (fd_ != -1) ::_close(fd_);
#else
  if (fd_ != -1) ::close(fd_);
#endif
  fd_ = fd;
}

void unix_pipe(unix_fd p[2]) {
  int pa[2];
#ifdef _WIN32
  if (::_pipe(pa, 64 * 1024, _O_BINARY) == -1)
#else
  if (::pipe(pa) == -1)
#endif
    throw std::system_error(errno, std::generic_category());
  p[0].reset(pa[0]);
  p[1].reset(pa[1]);
}
