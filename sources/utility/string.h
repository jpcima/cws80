#pragma once
#include "c++std/string_view"
#include <string>

std::string capitalize(cxx::string_view text);
