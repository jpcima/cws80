#pragma once

class unix_fd {
 public:
  constexpr unix_fd() noexcept {}
  explicit constexpr unix_fd(int fd) noexcept : fd_(fd) {}
  ~unix_fd() { reset(); }

  unix_fd(const unix_fd &) = delete;
  unix_fd &operator=(const unix_fd &) = delete;

  unix_fd(unix_fd &&o) noexcept : fd_(o.fd_) { o.fd_ = -1; }
  unix_fd &operator=(unix_fd &&o) noexcept { reset(o.fd_); o.fd_ = -1; return *this; }

  void reset(int fd = -1) noexcept;
  int get() const noexcept { return fd_; }
  int operator*() const noexcept { return fd_; }
  explicit operator bool() const noexcept  { return fd_ != -1; }

 private:
  int fd_ = -1;
};

void unix_pipe(unix_fd p[2]);
