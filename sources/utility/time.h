#pragma once
#include <chrono>

template <class TimePoint, class Duration>
bool elapsed_since(TimePoint start, Duration duration) {
  TimePoint now = TimePoint::clock::now();
  return now - start >= duration;
}
