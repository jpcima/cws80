#pragma once
#include "utility/types.h"
#include <c++std/string_view>
#include <string>
#include <memory>
#include <limits>
#include <type_traits>
#include <stdio.h>
#include <stdarg.h>

#ifndef STDFILE_NO_FMT
# include <fmt/format.h>
# include <system_error>
# include <errno.h>
#endif

#ifdef __GNUC__
# define STDFILE_ATTR_PRINTF(...) [[gnu::format(printf, __VA_ARGS__)]]
#else
# define STDFILE_ATTR_PRINTF(...)
#endif

class StdioFile {
 public:
  typedef size_t size_type;
#if defined(_WIN32)
  typedef u64 offset_type;
#else
  typedef std::make_unsigned_t<off_t> offset_type;
#endif
  static constexpr size_t size_max = std::numeric_limits<size_type>::max();

  StdioFile();
  StdioFile(const char *path, const char *mode);
  StdioFile(int fd, const char *mode, bool owned);
  StdioFile(const char *path, int oflag, int omode);

  void close();
  size_type read(void *ptr, size_type size);
  void write(const void *ptr, size_type size);
  int getc();
  void putc(char c);
  void puts(cxx::string_view s);
  bool getline(std::string &linebuf, size_type linemax = size_max);
  void flush();
  offset_type tell();
  void seek(offset_type offset, int whence);
  void truncate(offset_type size = 0);
  offset_type length();

#ifndef STDFILE_NO_FMT
  template <class... Args>
  void fmt(const char *fmt, const Args &... args) {
    FILE *fh = get();
    fmt::print(fh, fmt, args...);
    if (ferror(fh))
      throw std::system_error(EIO, std::generic_category());
  }
#endif

  STDFILE_ATTR_PRINTF(2, 3) uint printf(const char *fmt, ...);
  uint vprintf(const char *fmt, va_list ap);

  STDFILE_ATTR_PRINTF(2, 3) uint scanf(const char *fmt, ...);
  uint vscanf(const char *fmt, va_list ap);

  explicit operator bool() const;

  FILE *get() const;
  FILE *release();
  int fd() const;

  static StdioFile null();
  static StdioFile temporary();
  static StdioFile named_temporary(std::string &path);

  static StdioFile command(const char *command, const char *mode);
  static StdioFile command(const char *argv[], const char *mode);

  static bool remove_file(const char *path);
  static bool remove_directory(const char *path);

 private:
  std::unique_ptr<FILE, int(*)(FILE *)> handle_;
};
