#pragma once
#include "utility/stdfile.h"
#include "utility/types.h"

template <class Data>
class AudioFileOutput {
 public:
  explicit AudioFileOutput(StdioFile &file) : file_(file) {}

  void start(uint sample_rate, uint channel_count);
  void finish();

  void put(Data x);
  void write(const Data *x, uint n);

  template <class T> void put(T) = delete;

 private:
  StdioFile &file_;
};
