#include "gnuplot.h"
#include <stdlib.h>

StdioFile gp_open() {
  const char *arg0 = getenv("GNUPLOT");
  if (!arg0)
    arg0 = "gnuplot";
  const char *argv[] = {arg0, "-d", nullptr};
  return StdioFile::command(argv, "w");
}

std::string gp_escape(cxx::string_view str) {
  size_t len = str.size();
  std::string result;
  result.reserve(len * 2);
  for (size_t i = 0; i < len; ++i) {
    char buf[8];
    uint nchars = sprintf(buf, "\\%.3o", (u8)str[i]);
    result.append(buf, nchars);
  }
  return result;
}

void gp_xrange(StdioFile &gp, cxx::optional<f64> x1, cxx::optional<f64> x2) {
  gp.puts("set xrange [");
  if (x1)
    gp.printf("%.17e", *x1);
  gp.puts(" : ");
  if (x2)
    gp.printf("%.17e", *x2);
  gp.puts("]\n");
}

void gp_yrange(StdioFile &gp, cxx::optional<f64> y1, cxx::optional<f64> y2) {
  gp.puts("set yrange [");
  if (y1)
    gp.printf("%.17e", *y1);
  gp.puts(" : ");
  if (y2)
    gp.printf("%.17e", *y2);
  gp.puts("]\n");
}

void gp_y2range(StdioFile &gp, cxx::optional<f64> y1, cxx::optional<f64> y2) {
  gp.puts("set y2range [");
  if (y1)
    gp.printf("%.17e", *y1);
  gp.puts(" : ");
  if (y2)
    gp.printf("%.17e", *y2);
  gp.puts("]\n");
}

void gp_pause(StdioFile &gp) {
    gp.puts("pause mouse close\n");
}
