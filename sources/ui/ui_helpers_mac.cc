#include "ui/ui_helpers_mac.h"

namespace cws80 {

std::string NativeUI_Mac::choose_file(
    gsl::span<const FileChooserFilter> filters, const std::string &directory,
    const std::string &title, FileChooserMode mode) {
  // TODO
  return {};
}

//------------------------------------------------------------------------------
cxx::optional<std::string> NativeUI_Mac::edit_line(
    const std::string &title, const std::string &initial_value) {
  // TODO
  return {};
}

//------------------------------------------------------------------------------
opt_uint NativeUI_Mac::select_by_menu(
    gsl::span<const std::string> choices, opt_uint initial_selection) {
  // TODO
  return {};
}

}  // namespace cws80
