#include "ui/ui_helpers_gtk.h"
#include "utility/unix_fd.h"
#include "utility/scope_guard.h"
#include "utility/optional.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include <boost/lexical_cast.hpp>
#include <system_error>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>

namespace cws80 {

static const u8 gtk_helper_exe[] = {
#include "ui/data/gtk-helper.dat.h"
};

static std::string execute_helper(gsl::span<const std::string> args);

//------------------------------------------------------------------------------
std::string NativeUI_Gtk::choose_file(
    gsl::span<const FileChooserFilter> filters, const std::string &directory,
    const std::string &title, FileChooserMode mode) {
  std::vector<std::string> args;
  args.push_back("gtk-helper");
  args.push_back("choose-file");

  args.push_back("-m");
  switch (mode) {
    case FileChooserMode::Open: args.push_back("open"); break;
    case FileChooserMode::Save: args.push_back("save"); break;
    default: throw std::logic_error("invalid mode");
  }

  if (!directory.empty()) {
    args.push_back("-d");
    args.push_back(directory);
  }

  if (!title.empty()) {
    args.push_back("-t");
    args.push_back(title);
  }

  for (const FileChooserFilter &flt : filters) {
    args.push_back("-f");
    std::string arg;
    arg.append(flt.name);
    for (const char *pat : flt.patterns) {
      arg.push_back(',');
      arg.append(pat);
    }
    args.push_back(arg);
  }

  return execute_helper(args);
}

//------------------------------------------------------------------------------
cxx::optional<std::string> NativeUI_Gtk::edit_line(
    const std::string &title, const std::string &initial_value) {
  std::vector<std::string> args;
  args.push_back("gtk-helper");
  args.push_back("edit-line");

  if (!initial_value.empty()) {
    args.push_back("-v");
    args.push_back(initial_value);
  }

  if (!title.empty()) {
    args.push_back("-t");
    args.push_back(title);
  }

  std::string msg = execute_helper(args);

  char front = msg.size() ? msg.front() : '\0';
  switch (front) {
    case '+': return msg.substr(1);
    case '-': return {};
    default: throw std::logic_error("unrecognized message");
  }
}

//------------------------------------------------------------------------------
opt_uint NativeUI_Gtk::select_by_menu(
    gsl::span<const std::string> choices, opt_uint initial_selection) {
  std::vector<std::string> args;
  args.push_back("gtk-helper");
  args.push_back("select-option");

  if (initial_selection) {
    args.push_back("-v");
    args.push_back(std::to_string(*initial_selection));
  }

  args.push_back("--");
  args.insert(args.end(), choices.begin(), choices.end());

  std::string msg = execute_helper(args);
  if (msg.empty())
    return {};
  return boost::lexical_cast<uint>(msg);
}

//------------------------------------------------------------------------------
static unix_fd &get_executable_fd() {
  static unix_fd rfd_exe;
  if (!rfd_exe) {
    char path[] = P_tmpdir "/XXXXXX";
    unix_fd wfd_exe(mkostemp(path, O_WRONLY));
    if (!wfd_exe)
      throw std::system_error(errno, std::generic_category());
    SCOPE(exit) { if (path[0]) unlink(path); };

    if (fchmod(*wfd_exe, 0700) == -1)
      throw std::system_error(errno, std::generic_category());

    const u8 *data = gtk_helper_exe;
    size_t nb = sizeof(gtk_helper_exe);

    rfd_exe.reset(open(path, O_RDONLY|O_CLOEXEC));
    if (!rfd_exe)
      throw std::system_error(errno, std::generic_category());
    SCOPE(exit) { if (nb) rfd_exe.reset(); };

    unlink(path);
    path[0] = '\0';

    while (nb) {
      opt_size_t count = write(*wfd_exe, data, nb);
      if (!count) {
        if (errno == EINTR) continue;
        throw std::system_error(errno, std::generic_category());
      }
      nb -= *count;
      data += *count;
    }
  }
  return rfd_exe;
}

//------------------------------------------------------------------------------
static std::string process_get_result(pid_t pid, unix_fd &fd) {
  std::string msg;
  bool complete = false;
  msg.reserve(1024);

  opt_uint flags = fcntl(*fd, F_GETFL);
  if (!flags || fcntl(*fd, F_SETFL, *flags | O_NONBLOCK) == -1)
    throw std::system_error(errno, std::generic_category());

  while (!complete) {
    u8 buf[BUFSIZ];

    fd_set inset, exset;
    FD_ZERO(&inset);
    FD_SET(*fd, &inset);
    FD_ZERO(&exset);
    FD_SET(*fd, &exset);

    timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 50;

    int nfds = select(*fd + 1, &inset, nullptr, &exset, &timeout);
    if (nfds == -1) {
      if (errno == EINTR) continue;
      throw std::system_error(errno, std::generic_category());
    }

    if (FD_ISSET(*fd, &exset))
      throw std::runtime_error("process input error");

    if (FD_ISSET(*fd, &inset)) {
      opt_size_t count = read(*fd, buf, sizeof(buf));
      if (!count) {
        if (errno == EINTR) continue;
        throw std::system_error(errno, std::generic_category());
      }
      msg.append((char *)buf, *count);
    }

    int ws = 0;
    pid_t wret;
    do wret = waitpid(pid, &ws, WNOHANG);
    while (wret == -1 && errno == EINTR);

    if (wret == pid) {
      if (WIFSIGNALED(ws) || WEXITSTATUS(ws) != 0)
        throw std::runtime_error("subprocess failed to execute correctly");
      complete = true;
    }
  }
  fd.reset();

  return msg;
}

//------------------------------------------------------------------------------
static std::string execute_helper(gsl::span<const std::string> args) {
  size_t argc = args.size();
  dynarray<char *> argv(argc + 1);
  for (size_t i = 0; i < argc; ++i)
    argv[i] = (char *)args[i].c_str();
  argv[argc] = nullptr;

  static unix_fd &rfd_exe = get_executable_fd();

  unix_fd pfd[2];
  unix_pipe(pfd);

  pid_t pid = fork();
  if (pid == -1)
    throw std::system_error(errno, std::generic_category());

  if (pid == 0) {
    pfd[0].reset();
    if (dup2(*pfd[1], STDOUT_FILENO) == -1)
      throw std::system_error(errno, std::generic_category());
    if (lseek(*rfd_exe, 0, SEEK_SET) == -1)
      throw std::system_error(errno, std::generic_category());
    fexecve(*rfd_exe, argv.data(), environ);
    throw std::system_error(errno, std::generic_category());
  }

  pfd[1].reset();
  return process_get_result(pid, pfd[0]);
}

}  // namespace cws80
