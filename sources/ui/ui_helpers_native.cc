#include "ui/ui_helpers_native.h"
#if defined(_WIN32)
#include "ui/ui_helpers_win.h"
#elif defined(__APPLE__)
#include "ui/ui_helpers_mac.h"
#else
#include "ui/ui_helpers_gtk.h"
#endif

namespace cws80 {

NativeUI *NativeUI::create(void *window) {
#if defined(_WIN32)
  return new NativeUI_Win(window);
#elif defined(__APPLE__)
  return new NativeUI_Mac(window);
#else
  return new NativeUI_Gtk(window);
#endif
}

}  // namespace cws80
