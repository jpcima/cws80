#include "ui/ui_helpers_win.h"
#include "ui/ui_helpers_win.rc.h"
#include "utility/scope_guard.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include <fmt/format.h>
#include <windows.h>
#include <algorithm>
#include <memory>
#include <system_error>

namespace cws80 {

static HINSTANCE GetDllHandle() {
  static HMODULE hdll = NULL;
  if (!hdll) {
    DWORD dwflags = GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS|
                    GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT;
    LPCTSTR lpaddress = (LPCTSTR)&GetDllHandle;
    GetModuleHandleEx(dwflags, lpaddress, &hdll);
  }
  return hdll;
}

//------------------------------------------------------------------------------
std::string NativeUI_Win::choose_file(
    gsl::span<const FileChooserFilter> filters, const std::string &directory,
    const std::string &title, FileChooserMode mode) {
  OPENFILENAMEA ofn {};
  bool res = false;
  dynarray<char> lpfilename(MAX_PATH + 1);
  lpfilename[0] = '\0';

  //
  std::string ofnfilter;
  for (const FileChooserFilter &filter : filters) {
    ofnfilter.append(filter.name);
    ofnfilter.push_back('\0');
    for (size_t i = 0, n = filter.patterns.size(); i < n; ++i) {
      if (i > 0) ofnfilter.push_back(';');
      ofnfilter.append(filter.patterns[i]);
    }
    ofnfilter.push_back('\0');
  }
  ofnfilter.push_back('\0');

  //
  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = hwnd_;
  ofn.lpstrFilter = (ofnfilter.size() > 1) ? ofnfilter.data() : nullptr;
  ofn.lpstrFile = lpfilename.data();
  ofn.nMaxFile = MAX_PATH + 1;
  if (!directory.empty())
    ofn.lpstrInitialDir = directory.c_str();
  if (!title.empty())
    ofn.lpstrTitle = title.c_str();
  ofn.lpstrDefExt = "";
  ofn.Flags = OFN_HIDEREADONLY;

  //
  if (mode == FileChooserMode::Open) {
    ofn.Flags |= OFN_FILEMUSTEXIST;
    res = GetOpenFileNameA(&ofn);
  } else {
    ofn.Flags |= OFN_OVERWRITEPROMPT;
    res = GetSaveFileNameA(&ofn);
  }

  if (!res)
    return {};

  return ofn.lpstrFile;
}

//------------------------------------------------------------------------------
static std::string edit_line_title;
static std::string edit_line_value;

static INT_PTR CALLBACK edit_line_dialog_proc(
    HWND hdlg, UINT umsg, WPARAM wparam, LPARAM lparam) {
  switch (umsg) {
    case WM_INITDIALOG: {
      HWND hedit = GetDlgItem(hdlg, IDC_EDITLINE_EDIT);
      const std::string &title = edit_line_title;
      const std::string &value = edit_line_value;
      SetWindowTextA(hdlg, title.c_str());
      SetWindowTextA(hedit, value.c_str());
      return true;
    }
    case WM_COMMAND:
      switch (LOWORD(wparam)) {
        case ID_EDITLINE_OK: {
          HWND hedit = GetDlgItem(hdlg, IDC_EDITLINE_EDIT);
          char value[7];
          uint length = GetWindowTextA(hedit, value, 7);
          edit_line_value.assign(value, std::min(length, 6u));
          EndDialog(hdlg, wparam);
          return true;
        }
        case ID_EDITLINE_CANCEL:
          EndDialog(hdlg, wparam);
          return true;
        default:
          break;
      }
      break;
    default:
      break;
  }

  return false;
}

cxx::optional<std::string> NativeUI_Win::edit_line(
    const std::string &title, const std::string &initial_value) {
  HINSTANCE hdll = GetDllHandle();

  edit_line_title = title;
  edit_line_value = initial_value;

  INT_PTR dlgres = DialogBox(
      hdll, MAKEINTRESOURCE(IDD_EDITLINE_DIALOG), hwnd_,
      &edit_line_dialog_proc);
  if (dlgres == -1)
    throw std::system_error(GetLastError(), std::system_category());

  if (dlgres != ID_EDITLINE_OK)
    return {};

  return edit_line_value;
}

//------------------------------------------------------------------------------
opt_uint NativeUI_Win::select_by_menu(
    gsl::span<const std::string> choices, opt_uint initial_selection) {
  HWND hwnd = hwnd_;
  HMENU hmenu = CreatePopupMenu();
  if (!hmenu)
      throw std::system_error(GetLastError(), std::system_category());
  SCOPE(exit) { DestroyMenu(hmenu); };

  uint nchoices = choices.size();
  for (uint i = 0; i < nchoices; ++i) {
    if (!AppendMenuA(hmenu, MF_ENABLED|MF_STRING, i + 1, choices[i].c_str()))
      throw std::system_error(GetLastError(), std::system_category());
  }

  if (initial_selection) {
    if (*initial_selection >= nchoices)
      throw std::logic_error("invalid initial selection");
    SetMenuDefaultItem(hmenu, *initial_selection + 1, false);
    HiliteMenuItem(hwnd, hmenu, *initial_selection + 1, MF_BYCOMMAND|MF_HILITE);
  }

  POINT mousepos {};
  GetCursorPos(&mousepos);

  uint cmd = TrackPopupMenu(
      hmenu, TPM_VCENTERALIGN|TPM_LEFTALIGN|TPM_NONOTIFY|TPM_RETURNCMD,
      mousepos.x, mousepos.y, 0, hwnd, nullptr);

  if (cmd == 0)
    return {};
  return cmd - 1;
}

}  // namespace cws80
