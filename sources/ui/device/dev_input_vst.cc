#include "ui/device/dev_input_vst.h"

namespace cws80 {

static constexpr uint wheel_delta = 120;

void InputDevice_Vst::on_mouse_down(int button, int x, int y) {
  generic_mouse_down(translate_vst_button(button), x, y, 0);
}

void InputDevice_Vst::on_mouse_move(int x, int y) {
  generic_mouse_move(x, y);
}

void InputDevice_Vst::on_mouse_up(int button, int x, int y) {
  generic_mouse_up(translate_vst_button(button), x, y, 0);
}

void InputDevice_Vst::on_mouse_wheel(int wheel, int x, int y) {
  generic_mouse_wheel(wheel / (f64)wheel_delta, 0);
}

void InputDevice_Vst::on_key_down(const VstKeyCode &key) {
  bool special;
  u32 value;
  if (translate_vst_key(key, &special, &value)) {
    int mods = translate_vst_keymods(key.modifier);
    generic_key_down(value, special, mods);
  }
}

void InputDevice_Vst::on_key_up(const VstKeyCode &key) {
  bool special;
  u32 value;
  if (translate_vst_key(key, &special, &value)) {
    int mods = translate_vst_keymods(key.modifier);
    generic_key_up(value, special, mods);
  }
}

void InputDevice_Vst::on_focus_in() {
  generic_focus_in();
}

void InputDevice_Vst::on_focus_out() {
  generic_focus_out();
}

MButton InputDevice_Vst::translate_vst_button(int button) {
  return (MButton)((button == 2) ? 3 : (button == 3) ? 2 : button);
}

bool InputDevice_Vst::translate_vst_key(
    const VstKeyCode &key, bool *special, u32 *value) {
  if (key.character) {
    *value = key.character;
    *special = false;
    return true;
  }

  Key spk = translate_vst_keyvirt(key.virt);
  if (spk != (Key)0) {
    *value = (u32)spk;
    *special = true;
    return true;
  }

  return false;
}

int InputDevice_Vst::translate_vst_keymods(uint vstmods) {
  int uimods = 0;
  uimods |= (vstmods & MODIFIER_SHIFT) ? (uint)Kmod::Shift : 0;
  uimods |= (vstmods & MODIFIER_ALTERNATE) ? (uint)Kmod::Alt : 0;
  uimods |= (vstmods & MODIFIER_CONTROL) ? (uint)Kmod::Control : 0;
  uimods |= (vstmods & MODIFIER_COMMAND) ? (uint)Kmod::Super : 0;
  return uimods;
}

Key InputDevice_Vst::translate_vst_keyvirt(uint virt) {
  switch (virt) {
    case VKEY_F1: return Key::F1;
    case VKEY_F2: return Key::F2;
    case VKEY_F3: return Key::F3;
    case VKEY_F4: return Key::F4;
    case VKEY_F5: return Key::F5;
    case VKEY_F6: return Key::F6;
    case VKEY_F7: return Key::F7;
    case VKEY_F8: return Key::F8;
    case VKEY_F9: return Key::F9;
    case VKEY_F10: return Key::F10;
    case VKEY_F11: return Key::F11;
    case VKEY_F12: return Key::F12;
    case VKEY_LEFT: return Key::Left;
    case VKEY_UP: return Key::Up;
    case VKEY_RIGHT: return Key::Right;
    case VKEY_DOWN: return Key::Down;
    case VKEY_PAGEUP: return Key::PageUp;
    case VKEY_PAGEDOWN: return Key::PageDown;
    case VKEY_HOME: return Key::Home;
    case VKEY_END: return Key::End;
    case VKEY_INSERT: return Key::Insert;
    case VKEY_SHIFT: return Key::Shift;
    case VKEY_CONTROL: return Key::Ctrl;
    case VKEY_ALT: return Key::Alt;
    // case VKEY_: return Key::Super;
    default: return (Key)0;
  }
}

}  // namespace cws80
