#include "dev_graphics_gdip.h"
#include <fmt/format.h>
#include "cws80_ui_controller.h"
#include "cws80_ui_nk.h"
#include "utility/scope_guard.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include <fmt/format.h>
#include <windows.h>
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#include <gdiplus.h>  // requires `min` and `max` macros
#undef min
#undef max
#include <boost/locale/encoding_utf.hpp>
#include <c++std/optional>
#include <array>
#include <system_error>
#include <assert.h>

using namespace Gdiplus;
using namespace Gdiplus::DllExports;

using boost::locale::conv::utf_to_utf;

namespace cws80 {

struct GpPrivateFontCollection : GpFontCollection {};

struct GpDeleter;
template <class T> using GpRef = std::unique_ptr<T, GpDeleter>;

struct GpDeleter {
  void operator()(GpGraphics *x) { GdipDeleteGraphics(x); }
  void operator()(GpImage *x) { GdipDisposeImage(x); }
  void operator()(GpPen *x) { GdipDeletePen(x); }
  void operator()(GpBrush *x) { GdipDeleteBrush(x); }
  void operator()(GpStringFormat *x) { GdipDeleteStringFormat(x); }
  void operator()(GpFont *x) { GdipDeleteFont(x); }
  void operator()(GpPrivateFontCollection *x) { GdipDeletePrivateFontCollection((GpFontCollection **)&x); }
};

struct Font {
  struct nk_user_font nk {};
  GpRef<GpFont> gp;
  void *userdata = nullptr;
};

struct GraphicsDevice_Gdip::Impl {
  GraphicsDevice_Gdip *Q = nullptr;
  void *hwnd_ = nullptr;
  cxx::optional<ULONG_PTR> token_;
  GpRef<GpGraphics> window_;
  GpRef<GpGraphics> memory_;
  GpRef<GpBitmap> bitmap_;
  GpRef<GpPen> pen_;
  GpRef<GpSolidFill> brush_;
  GpRef<GpStringFormat> format_;
  std::vector<GpRef<GpBitmap>> images_;
  std::vector<GpRef<GpPrivateFontCollection>> collections_;
  std::vector<std::unique_ptr<Font>> fonts_;

  void load_fonts(gsl::span<const FontRequest> fontreqs, const nk_rune range[]);
  void create_font_from_collection(GpFontCollection *coll, f32 size);
  static f32 text_width(nk_handle handle, f32 height, const char *text, int len);

  static ARGB convert_color(nk_color c);
  void scissor(f32 x, f32 y, f32 w, f32 h);
  void stroke_line(i16 x0, i16 y0, i16 x1, i16 y1, uint line_thickness, nk_color col);
  void stroke_rect(i16 x, i16 y, u16 w, u16 h, u16 r, u16 line_thickness, nk_color col);
  void fill_rect(i16 x, i16 y, u16 w, u16 h, u16 r, nk_color col);
  void fill_triangle(i16 x0, i16 y0, i16 x1, i16 y1, i16 x2, i16 y2, nk_color col);
  void stroke_triangle(i16 x0, i16 y0, i16 x1, i16 y1, i16 x2, i16 y2, u16 line_thickness, nk_color col);
  void fill_polygon(const struct nk_vec2i *pnts, int count, nk_color col);
  void stroke_polygon(const struct nk_vec2i *pnts, int count, u16 line_thickness, nk_color col);
  void stroke_polyline(const struct nk_vec2i *pnts, int count, u16 line_thickness, nk_color col);
  void fill_circle(i16 x, i16 y, u16 w, u16 h, nk_color col);
  void stroke_circle(i16 x, i16 y, u16 w, u16 h, u16 line_thickness, nk_color col);
  void stroke_curve(struct nk_vec2i p1, struct nk_vec2i p2, struct nk_vec2i p3, struct nk_vec2i p4, u16 line_thickness, nk_color col);
  void draw_text(i16 x, i16 y, u16 w, u16 h, const char *text, int len, Font *font, nk_color cbg, nk_color cfg);
  void draw_image(i16 x, i16 y, u16 w, u16 h, struct nk_image img, nk_color col);
  static void save_png(GpImage *img, const char *path);
  void clear(nk_color col);
  void blit(GpGraphics *graphics);
  void prerender_gui(nk_anti_aliasing aa);
};

static void check(GpStatus status);

GraphicsDevice_Gdip::GraphicsDevice_Gdip(UIController &ctl, void *hwnd)
    : GraphicsDevice(ctl), P(new Impl) {
  P->Q = this;
  P->hwnd_ = hwnd;
}

GraphicsDevice_Gdip::~GraphicsDevice_Gdip() {}

void GraphicsDevice_Gdip::initialize(
    gsl::span<const FontRequest> fontreqs, const nk_rune range[]) {
  bool success = false;
  assert(!P->token_);
  SCOPE(exit) { if (!success) cleanup(); };

  NkScreen &screen = ctl_.screen();
  uint w = screen.width();
  uint h = screen.height();
  HWND hwnd = (HWND)P->hwnd_;

  GdiplusStartupInput startup(NULL, FALSE, TRUE);
  ULONG_PTR token = 0;
  if (GdiplusStartup(&token, &startup, NULL) != Ok)
    throw std::runtime_error("cannot initialize GDI+");
  P->token_ = token;

  GpGraphics *window = nullptr;
  GpBitmap *bitmap = nullptr;
  GpGraphics *memory = nullptr;
  GpPen *pen = nullptr;
  GpSolidFill *brush = nullptr;
  GpStringFormat *format = nullptr;

  check(GdipCreateFromHWND(hwnd, &window));
  P->window_.reset(window);
  check(GdipCreateBitmapFromGraphics(w, h, window, &bitmap));
  P->bitmap_.reset(bitmap);
  check(GdipGetImageGraphicsContext(bitmap, &memory));
  P->memory_.reset(memory);
  check(GdipCreatePen1(0, 1.0f, UnitPixel, &pen));
  P->pen_.reset(pen);
  check(GdipCreateSolidFill(0, &brush));
  P->brush_.reset(brush);
  check(GdipStringFormatGetGenericTypographic(&format));
  P->format_.reset(format);
  GdipSetStringFormatFlags(
      format, StringFormatFlagsNoFitBlackBox |
      StringFormatFlagsMeasureTrailingSpaces |
      StringFormatFlagsNoWrap | StringFormatFlagsNoClip);

  P->load_fonts(fontreqs, range);

  success = true;
}

void GraphicsDevice_Gdip::cleanup() {
  if (!P->token_)
    return;
  P->fonts_.clear();
  P->collections_.clear();
  P->images_.clear();
  P->window_.reset();
  P->memory_.reset();
  P->bitmap_.reset();
  P->pen_.reset();
  P->brush_.reset();
  P->format_.reset();
  GdiplusShutdown(*P->token_);
  P->token_ = {};
}

im_texture GraphicsDevice_Gdip::load_texture(
    const u8 *data, uint w, uint h, uint channels) {
  GpBitmap *bitmap = nullptr;

  size_t id = 0, idmax = P->images_.size();
  while (id < idmax && P->images_[id]) ++id;
  if (id == idmax) P->images_.emplace_back();

  PixelFormat fmt = PixelFormatUndefined;
  size_t size = w * h;
  dynarray<u8> argb(size * 4);

  switch (channels) {
    case 3:  // must convert RGB -> ARGB and byteswap
      for (size_t i = 0; i < size; ++i) {
        const u8 *src = &data[i*3];
        u8 *dst = &argb[i*4];
        dst[0] = src[2];  // B
        dst[1] = src[1];  // G
        dst[2] = src[0];  // R
        dst[3] = 0xff;
      }
      fmt = PixelFormat32bppARGB;
      break;
    case 4: {  // must convert RGBA -> ARGB and byteswap
      for (size_t i = 0; i < size; ++i) {
        const u8 *src = &data[i*4];
        u8 *dst = &argb[i*4];
        dst[0] = src[2];  // B
        dst[1] = src[1];  // G
        dst[2] = src[0];  // R
        dst[3] = src[3];  // A
      }
      fmt = PixelFormat32bppARGB;
      break;
    }
    default:
      break;
  }

  if (fmt == PixelFormatUndefined)
    throw std::runtime_error("unsupported image format for GDI+");

  GpBitmap *scan0bitmap = nullptr;
  check(GdipCreateBitmapFromScan0(w, h, w * 4, fmt, argb.data(), &scan0bitmap));
  SCOPE(exit) { GdipDisposeImage(scan0bitmap); };

  check(GdipCloneBitmapAreaI(0, 0, w, h, fmt, scan0bitmap, &bitmap));

  im_texture tex = nk_image_id(id);
  tex->h = h;
  tex->w = w;
  tex->region[2] = w;
  tex->region[3] = h;

  P->images_[id].reset(bitmap);
  return tex;
}

void GraphicsDevice_Gdip::unload_texture(nk_handle handle) {
  P->images_.at(handle.id).reset();
}

void GraphicsDevice_Gdip::render() {
  HWND hwnd = (HWND)P->hwnd_;

  P->prerender_gui(NK_ANTI_ALIASING_ON);
  PAINTSTRUCT paint;
  HDC dc = BeginPaint(hwnd, &paint);
  GpGraphics *graphics = nullptr;
  check(GdipCreateFromHDC(dc, &graphics));
  GpRef<GpGraphics> ref_graphics(graphics);
  P->blit(graphics);
  EndPaint(hwnd, &paint);

  NkScreen &screen = ctl_.screen();
  nk_context *ctx = screen.context();
  nk_clear(ctx);
}

nk_user_font *GraphicsDevice_Gdip::get_font(uint id) {
  std::vector<std::unique_ptr<Font>> &fonts = P->fonts_;
  if (id >= fonts.size())
    return nullptr;
  return &fonts[id]->nk;
}

void GraphicsDevice_Gdip::Impl::load_fonts(
    gsl::span<const FontRequest> fontreqs, const nk_rune range[]) {
  std::vector<GpRef<GpPrivateFontCollection>> &collections = collections_;
  fonts_.reserve(fontreqs.size());

  for (const FontRequest &req : fontreqs) {
    GpPrivateFontCollection *coll = nullptr;
    check(GdipNewPrivateFontCollection((GpFontCollection **)&coll));
    collections.emplace_back(GpRef<GpPrivateFontCollection>(coll));

    switch (req.type) {
      case FontRequest::Type::Default:
        // not supported
        break;
      case FontRequest::Type::File: {
        const std::string &path = req.un.file.path;
        std::wstring wpath = utf_to_utf<wchar_t>(path);
        check(GdipPrivateAddFontFile(coll, wpath.c_str()));
        break;
      }
      case FontRequest::Type::Memory:
        check(GdipPrivateAddMemoryFont(coll, (void *)req.un.memory.data, req.un.memory.size));
        break;
      default:
        assert(false);
    }

    create_font_from_collection(coll, req.height);
  }
}

void GraphicsDevice_Gdip::Impl::create_font_from_collection(
    GpFontCollection *coll, f32 size) {
  std::unique_ptr<Font> font(new Font);
  font->userdata = this;

  uint count = 0;
  check(GdipGetFontCollectionFamilyCount(coll, (int *)&count));

  dynarray<GpFontFamily *> families(count);
  check(GdipGetFontCollectionFamilyList(coll, count, families.data(), (int *)&count));

  if (count < 1)
    throw std::runtime_error("could not load font");

  GpFont *gpfont = nullptr;
  GpFontFamily *fam = families[count - 1];

  FontStyle style = FontStyleRegular;

  const std::array<FontStyle, 4> allstyles {{
      FontStyleRegular, FontStyleBold, FontStyleItalic, FontStyleBoldItalic }};

  BOOL havestyle = false;
  for (uint i = 0; !havestyle && i < allstyles.size(); ++i) {
    GdipIsStyleAvailable(fam, allstyles[i], &havestyle);
    if (havestyle)
      style = allstyles[i];
  }

  check(GdipCreateFont(fam, size, style, UnitPixel, &gpfont));

  font->gp.reset(gpfont);

  nk_user_font &nk = font->nk;
  nk.height = size;
  nk.width = &text_width;
  nk.userdata = nk_handle_ptr(font.get());

  fonts_.emplace_back(std::move(font));
}

f32 GraphicsDevice_Gdip::Impl::text_width(
    nk_handle handle, f32 height, const char *text, int len)
{
  Font *font = (Font *)handle.ptr;
  RectF layout = { 0.0f, 0.0f, 65536.0f, 65536.0f };
  if (!font || !text)
    return 0;

  Impl *self = (Impl *)font->userdata;
  std::wstring wtext = utf_to_utf<wchar_t>(text, text + (uint)len);

  RectF bbox {};
  check(GdipMeasureString(
      self->memory_.get(), wtext.data(), wtext.length(), font->gp.get(), &layout,
      self->format_.get(), &bbox, nullptr, nullptr));

  return bbox.Width;
}

//------------------------------------------------------------------------------
ARGB GraphicsDevice_Gdip::Impl::convert_color(nk_color c) {
  return (c.a << 24) | (c.r << 16) | (c.g << 8) | c.b;
}

void GraphicsDevice_Gdip::Impl::scissor(f32 x, f32 y, f32 w, f32 h) {
  GdipSetClipRectI(memory_.get(), (INT)x, (INT)y, (INT)(w + 1), (INT)(h + 1),
                   CombineModeReplace);
}

void GraphicsDevice_Gdip::Impl::stroke_line(
    i16 x0, i16 y0, i16 x1, i16 y1, uint line_thickness, nk_color col) {
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  GdipDrawLineI(memory_.get(), pen_.get(), x0, y0, x1, y1);
}

void GraphicsDevice_Gdip::Impl::stroke_rect(
    i16 x, i16 y, u16 w, u16 h, u16 r, u16 line_thickness, nk_color col) {
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  if (r == 0) {
    GdipDrawRectangleI(memory_.get(), pen_.get(), x, y, w, h);
  } else {
    int d = 2 * r;
    GdipDrawArcI(memory_.get(), pen_.get(), x, y, d, d, 180, 90);
    GdipDrawLineI(memory_.get(), pen_.get(), x + r, y, x + w - r, y);
    GdipDrawArcI(memory_.get(), pen_.get(), x + w - d, y, d, d, 270, 90);
    GdipDrawLineI(memory_.get(), pen_.get(), x + w, y + r, x + w, y + h - r);
    GdipDrawArcI(memory_.get(), pen_.get(), x + w - d, y + h - d, d, d, 0, 90);
    GdipDrawLineI(memory_.get(), pen_.get(), x, y + r, x, y + h - r);
    GdipDrawArcI(memory_.get(), pen_.get(), x, y + h - d, d, d, 90, 90);
    GdipDrawLineI(memory_.get(), pen_.get(), x + r, y + h, x + w - r, y + h);
  }
}

void GraphicsDevice_Gdip::Impl::fill_rect(
    i16 x, i16 y, u16 w, u16 h, u16 r, nk_color col) {
  GdipSetSolidFillColor(brush_.get(), convert_color(col));
  if (r == 0) {
    GdipFillRectangleI(memory_.get(), brush_.get(), x, y, w, h);
  } else {
    int d = 2 * r;
    GdipFillRectangleI(memory_.get(), brush_.get(), x + r, y, w - d, h);
    GdipFillRectangleI(memory_.get(), brush_.get(), x, y + r, r, h - d);
    GdipFillRectangleI(memory_.get(), brush_.get(), x + w - r, y + r, r, h - d);
    GdipFillPieI(memory_.get(), brush_.get(), x, y, d, d, 180, 90);
    GdipFillPieI(memory_.get(), brush_.get(), x + w - d, y, d, d, 270, 90);
    GdipFillPieI(memory_.get(), brush_.get(), x + w - d, y + h - d, d, d, 0, 90);
    GdipFillPieI(memory_.get(), brush_.get(), x, y + h - d, d, d, 90, 90);
  }
}

void GraphicsDevice_Gdip::Impl::fill_triangle(
    i16 x0, i16 y0, i16 x1, i16 y1, i16 x2, i16 y2, nk_color col) {
  GpPoint points[] = {
      {x0, y0},
      {x1, y1},
      {x2, y2},
  };

  GdipSetSolidFillColor(brush_.get(), convert_color(col));
  GdipFillPolygonI(memory_.get(), brush_.get(), points, 3, FillModeAlternate);
}

void GraphicsDevice_Gdip::Impl::stroke_triangle(
    i16 x0, i16 y0, i16 x1, i16 y1, i16 x2, i16 y2, u16 line_thickness, nk_color col) {
  GpPoint points[] = {
      {x0, y0},
      {x1, y1},
      {x2, y2},
      {x0, y0},
  };
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  GdipDrawPolygonI(memory_.get(), pen_.get(), points, 4);
}

void GraphicsDevice_Gdip::Impl::fill_polygon(
    const struct nk_vec2i *pnts, int count, nk_color col) {
  int i = 0;
  static constexpr int max_points = 64;
  GpPoint points[max_points];
  GdipSetSolidFillColor(brush_.get(), convert_color(col));
  for (i = 0; i < count && i < max_points; ++i) {
    points[i].X = pnts[i].x;
    points[i].Y = pnts[i].y;
  }
  GdipFillPolygonI(memory_.get(), brush_.get(), points, i, FillModeAlternate);
}

void GraphicsDevice_Gdip::Impl::stroke_polygon(
    const struct nk_vec2i *pnts, int count, u16 line_thickness, nk_color col) {
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  if (count > 0) {
    int i;
    for (i = 1; i < count; ++i)
      GdipDrawLineI(memory_.get(), pen_.get(), pnts[i - 1].x, pnts[i - 1].y,
                    pnts[i].x, pnts[i].y);
    GdipDrawLineI(memory_.get(), pen_.get(), pnts[count - 1].x, pnts[count - 1].y,
                  pnts[0].x, pnts[0].y);
  }
}

void GraphicsDevice_Gdip::Impl::stroke_polyline(
    const struct nk_vec2i *pnts, int count, u16 line_thickness, nk_color col) {
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  if (count > 0) {
    int i;
    for (i = 1; i < count; ++i)
      GdipDrawLineI(memory_.get(), pen_.get(), pnts[i - 1].x, pnts[i - 1].y,
                    pnts[i].x, pnts[i].y);
  }
}

void GraphicsDevice_Gdip::Impl::fill_circle(
    i16 x, i16 y, u16 w, u16 h, nk_color col) {
  GdipSetSolidFillColor(brush_.get(), convert_color(col));
  GdipFillEllipseI(memory_.get(), brush_.get(), x, y, w, h);
}

void GraphicsDevice_Gdip::Impl::stroke_circle(
    i16 x, i16 y, u16 w, u16 h, u16 line_thickness, nk_color col) {
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  GdipDrawEllipseI(memory_.get(), pen_.get(), x, y, w, h);
}

void GraphicsDevice_Gdip::Impl::stroke_curve(
    struct nk_vec2i p1, struct nk_vec2i p2, struct nk_vec2i p3, struct nk_vec2i p4,
    u16 line_thickness, nk_color col) {
  GdipSetPenWidth(pen_.get(), (REAL)line_thickness);
  GdipSetPenColor(pen_.get(), convert_color(col));
  GdipDrawBezierI(memory_.get(), pen_.get(), p1.x, p1.y, p2.x, p2.y, p3.x, p3.y,
                  p4.x, p4.y);
}

void GraphicsDevice_Gdip::Impl::draw_text(
    i16 x, i16 y, u16 w, u16 h, const char *text, int len,
    Font *font, nk_color cbg, nk_color cfg) {
  RectF layout = {(f32)x, (f32)y, (f32)w, (f32)h};

  if (!text || !font || !len) return;

  std::wstring wtext = utf_to_utf<wchar_t>(text, text + (uint)len);
  GdipSetSolidFillColor(brush_.get(), convert_color(cfg));
  GdipDrawString(
      memory_.get(), wtext.data(), wtext.length(), font->gp.get(), &layout, format_.get(),
      brush_.get());
}

void GraphicsDevice_Gdip::Impl::draw_image(
    i16 x, i16 y, u16 w, u16 h, struct nk_image img, nk_color col) {
  GpImage *image = images_.at(img.handle.id).get();
  GdipDrawImageRectI(memory_.get(), image, x, y, w, h);
}

void GraphicsDevice_Gdip::Impl::save_png(GpImage *img, const char *path) {
  static cxx::optional<CLSID> clsid;
  cxx::wstring_view mimetype = L"image/png";

  if (!clsid) {
    uint count = 0, size = 0;
    GetImageEncodersSize(&count, &size);

    dynarray<u8> buf(size);
    ImageCodecInfo *info = (ImageCodecInfo *)buf.data();
    GetImageEncoders(count, size, info);

    ImageCodecInfo *end = info + count;
    ImageCodecInfo *codec = std::find_if(
        info, end, [&](const ImageCodecInfo &x) -> bool {
          return mimetype == x.MimeType; });
    if (codec != end)
      clsid = codec->Clsid;
  }

  if (!clsid)
    throw std::runtime_error("cannot find image encoder");

  std::wstring wpath = utf_to_utf<wchar_t>(path);
  check(GdipSaveImageToFile(img, wpath.c_str(), &*clsid, nullptr));
}

void GraphicsDevice_Gdip::Impl::clear(nk_color col) {
  GdipGraphicsClear(memory_.get(), convert_color(col));
}

void GraphicsDevice_Gdip::Impl::blit(GpGraphics *graphics) {
  // save_png(bitmap_.get(), "C:\\blit.png");
  GdipDrawImageI(graphics, bitmap_.get(), 0, 0);
}

//------------------------------------------------------------------------------
void GraphicsDevice_Gdip::Impl::prerender_gui(nk_anti_aliasing aa) {
  NkScreen &screen = Q->ctl_.screen();
  nk_context *ctx = screen.context();

  const struct nk_command *cmd;

  GdipSetTextRenderingHint(memory_.get(), aa != NK_ANTI_ALIASING_OFF ?
                           TextRenderingHintClearTypeGridFit : TextRenderingHintSingleBitPerPixelGridFit);
  GdipSetSmoothingMode(memory_.get(), aa != NK_ANTI_ALIASING_OFF ?
                       SmoothingModeHighQuality : SmoothingModeNone);

  nk_foreach(cmd, ctx)
  {
    switch (cmd->type) {
      case NK_COMMAND_NOP: break;
      case NK_COMMAND_SCISSOR: {
        const struct nk_command_scissor *s =(const struct nk_command_scissor*)cmd;
        scissor(s->x, s->y, s->w, s->h);
      } break;
      case NK_COMMAND_LINE: {
        const struct nk_command_line *l = (const struct nk_command_line *)cmd;
        stroke_line(l->begin.x, l->begin.y, l->end.x,
                    l->end.y, l->line_thickness, l->color);
      } break;
      case NK_COMMAND_RECT: {
        const struct nk_command_rect *r = (const struct nk_command_rect *)cmd;
        stroke_rect(r->x, r->y, r->w, r->h,
                    (ushort)r->rounding, r->line_thickness, r->color);
      } break;
      case NK_COMMAND_RECT_FILLED: {
        const struct nk_command_rect_filled *r = (const struct nk_command_rect_filled *)cmd;
        fill_rect(r->x, r->y, r->w, r->h,
                  (ushort)r->rounding, r->color);
      } break;
      case NK_COMMAND_CIRCLE: {
        const struct nk_command_circle *c = (const struct nk_command_circle *)cmd;
        stroke_circle(c->x, c->y, c->w, c->h, c->line_thickness, c->color);
      } break;
      case NK_COMMAND_CIRCLE_FILLED: {
        const struct nk_command_circle_filled *c = (const struct nk_command_circle_filled *)cmd;
        fill_circle(c->x, c->y, c->w, c->h, c->color);
      } break;
      case NK_COMMAND_TRIANGLE: {
        const struct nk_command_triangle*t = (const struct nk_command_triangle*)cmd;
        stroke_triangle(t->a.x, t->a.y, t->b.x, t->b.y,
                        t->c.x, t->c.y, t->line_thickness, t->color);
      } break;
      case NK_COMMAND_TRIANGLE_FILLED: {
        const struct nk_command_triangle_filled *t = (const struct nk_command_triangle_filled *)cmd;
        fill_triangle(t->a.x, t->a.y, t->b.x, t->b.y,
                      t->c.x, t->c.y, t->color);
      } break;
      case NK_COMMAND_POLYGON: {
        const struct nk_command_polygon *p =(const struct nk_command_polygon*)cmd;
        stroke_polygon(p->points, p->point_count, p->line_thickness,p->color);
      } break;
      case NK_COMMAND_POLYGON_FILLED: {
        const struct nk_command_polygon_filled *p = (const struct nk_command_polygon_filled *)cmd;
        fill_polygon(p->points, p->point_count, p->color);
      } break;
      case NK_COMMAND_POLYLINE: {
        const struct nk_command_polyline *p = (const struct nk_command_polyline *)cmd;
        stroke_polyline(p->points, p->point_count, p->line_thickness, p->color);
      } break;
      case NK_COMMAND_TEXT: {
        const struct nk_command_text *t = (const struct nk_command_text*)cmd;
        Font *font = (Font *)t->font->userdata.ptr;
        draw_text(t->x, t->y, t->w, t->h,
                  (const char*)t->string, t->length,
                  font, t->background, t->foreground);
      } break;
      case NK_COMMAND_CURVE: {
        const struct nk_command_curve *q = (const struct nk_command_curve *)cmd;
        stroke_curve(q->begin, q->ctrl[0], q->ctrl[1],
                     q->end, q->line_thickness, q->color);
      } break;
      case NK_COMMAND_IMAGE: {
        const struct nk_command_image *i = (const struct nk_command_image *)cmd;
        draw_image(i->x, i->y, i->w, i->h, i->img, i->col);
      } break;
      case NK_COMMAND_RECT_MULTI_COLOR:
      case NK_COMMAND_ARC:
      case NK_COMMAND_ARC_FILLED:
      default: break;
    }
  }
}

//------------------------------------------------------------------------------
struct gdip_error_category : public std::error_category {
 public:
  using std::error_category::error_category;

  static const std::array<const char *, 22> messages;

  const char *name() const noexcept override;
  std::string message(int condition) const override;
};

const char *gdip_error_category::name() const noexcept {
  return "GDI+";
}

std::string gdip_error_category::message(int condition) const {
  const uint count = sizeof(messages) / sizeof(messages[0]);
  if ((uint)condition < count)
    return messages[condition];
  return "unknown error";
}

const std::array<const char *, 22> gdip_error_category::messages {{
  "Ok",
  "generic error",
  "invalid parameter",
  "out of memory",
  "object busy",
  "insufficient buffer",
  "not implemented",
  "Win32 error",
  "wrong state",
  "aborted",
  "file not found",
  "value overflow",
  "access denied",
  "unknown image format",
  "font family not found",
  "font style not found",
  "not true type font",
  "unsupported GDI+ version",
  "GDI+ not initialized",
  "property not found",
  "property not supported",
  "profile not found"
}};

static void check(GpStatus status) {
  if (status != Ok)
    throw std::system_error(status, gdip_error_category());
}

}  // namespace cws80
