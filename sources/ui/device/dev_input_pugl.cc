#include "ui/device/dev_input_pugl.h"

namespace cws80 {

bool InputDevice_Pugl::on_event(const PuglEvent &evt) {
  switch (evt.type) {
    case PUGL_KEY_PRESS:
      if (!evt.key.filter) {
        if (evt.key.character && evt.key.keycode < 256) {
          keychars_[evt.key.keycode] = evt.key.character;
          generic_key_down(evt.key.character, evt.key.state, false);
        } else
          generic_key_down(evt.key.special, evt.key.state, true);
      }
      return true;

    case PUGL_KEY_RELEASE:
      if (!evt.key.filter) {
        if (evt.key.keycode < 256 && keychars_[evt.key.keycode]) {
          generic_key_up(keychars_[evt.key.keycode], evt.key.state, false);
          keychars_[evt.key.keycode] = 0;
        } else
          generic_key_up(evt.key.special, evt.key.state, true);
      }
      return true;

    case PUGL_MOTION_NOTIFY:
      generic_mouse_move(evt.motion.x, evt.motion.y);
      return true;

    case PUGL_BUTTON_PRESS:
      generic_mouse_down((MButton)evt.button.button, evt.button.x, evt.button.y, evt.button.state);
      return true;

    case PUGL_BUTTON_RELEASE:
      generic_mouse_up((MButton)evt.button.button, evt.button.x, evt.button.y, evt.button.state);
      return true;

    case PUGL_SCROLL:
      generic_mouse_wheel(evt.scroll.dy, evt.scroll.state);
      return true;

    case PUGL_FOCUS_IN:
      generic_focus_in();
      return true;

    case PUGL_FOCUS_OUT:
      generic_focus_out();
      return true;

    default:
      return false;
  }
}

}  // namespace cws80
