#pragma once
#include <pluginterfaces/vst2.x/aeffectx.h>
#include "ui/device/dev_input.h"

namespace cws80 {

class InputDevice_Vst : public InputDevice {
 public:
  using InputDevice::InputDevice;
  void on_mouse_down(int button, int x, int y);
  void on_mouse_move(int x, int y);
  void on_mouse_up(int button, int x, int y);
  void on_mouse_wheel(int wheel, int x, int y);
  void on_key_down(const VstKeyCode &key);
  void on_key_up(const VstKeyCode &key);
  void on_focus_in();
  void on_focus_out();
 private:
  static MButton translate_vst_button(int button);
  static bool translate_vst_key(const VstKeyCode &key, bool *special, u32 *value);
  static int translate_vst_keymods(uint vstmods);
  static Key translate_vst_keyvirt(uint virt);
};

}  // namespace cws80
