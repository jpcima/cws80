#pragma once
#include <pugl/pugl.h>
#include "ui/device/dev_input.h"
#include "utility/types.h"

namespace cws80 {

class InputDevice_Pugl : public InputDevice {
 public:
  using InputDevice::InputDevice;
  bool on_event(const PuglEvent &evt);

 private:
  u32 keychars_[256] = {};
};

}  // namespace cws80
