#pragma once
#include "ui/device/dev_graphics.h"
#include <memory>
#include <vector>

//
namespace cws80 {

class GraphicsDevice_Gdip : public GraphicsDevice {
 public:
  using GraphicsDevice::GraphicsDevice;;

  explicit GraphicsDevice_Gdip(UIController &ctl, void *hwnd);
  ~GraphicsDevice_Gdip();

  inline GraphicsType type() const override { return GraphicsType::Gdiplus; }

  void initialize(
      gsl::span<const FontRequest> fontreqs, const nk_rune range[]) override;
  void cleanup() override;

  im_texture load_texture(const u8 *data, uint w, uint h, uint channels) override;
  void unload_texture(nk_handle handle) override;

  void render() override;

  nk_user_font *get_font(uint id) override;

 private:
  struct Impl;
  std::unique_ptr<Impl> P;
};

}  // namespace cws80
