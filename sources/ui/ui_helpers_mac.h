#pragma once
#include "ui/ui_helpers_native.h"

namespace cws80 {

class NativeUI_Mac : public NativeUI {
 public:
  explicit NativeUI_Mac(void *window) {}

  std::string choose_file(
      gsl::span<const FileChooserFilter> filters, const std::string &directory,
      const std::string &title, FileChooserMode mode) override;

  cxx::optional<std::string> edit_line(
      const std::string &title, const std::string &initial_value) override;

  opt_uint select_by_menu(
      gsl::span<const std::string> choices, opt_uint initial_selection) override;
};

}  // namespace cws80
