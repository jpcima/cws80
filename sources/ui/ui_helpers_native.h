#pragma once
#include "utility/optional.h"
#include "utility/types.h"
#include <c++std/optional>
#include <gsl/span>
#include <vector>
#include <string>

namespace cws80 {

enum class FileChooserMode {
  Open, Save,
};

struct FileChooserFilter {
  const char *name;
  std::vector<const char *> patterns;
};

class NativeUI {
 public:
  ~NativeUI() {}

  static NativeUI *create(void *window);

  virtual std::string choose_file(
      gsl::span<const FileChooserFilter> filters, const std::string &directory,
      const std::string &title, FileChooserMode mode) = 0;

  virtual cxx::optional<std::string> edit_line(
      const std::string &title, const std::string &initial_value) = 0;

  virtual opt_uint select_by_menu(
      gsl::span<const std::string> choices, opt_uint initial_selection) = 0;
};

}  // namespace cws80
