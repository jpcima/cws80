#pragma once
#include "ui/ui_helpers_native.h"
#include <windows.h>

namespace cws80 {

class NativeUI_Win : public NativeUI {
 public:
  explicit NativeUI_Win(void *window) : hwnd_((HWND)window) {}

  std::string choose_file(
      gsl::span<const FileChooserFilter> filters, const std::string &directory,
      const std::string &title, FileChooserMode mode) override;

  cxx::optional<std::string> edit_line(
      const std::string &title, const std::string &initial_value) override;

  opt_uint select_by_menu(
      gsl::span<const std::string> choices, opt_uint initial_selection) override;

 private:
  HWND hwnd_ = nullptr;
};

}  // namespace cws80
