#include "ui/ui_helpers_vst.h"
#include "utility/scope_guard.h"
#include "utility/debug.h"
#include <vstgui/vstgui.h>
#include <vstgui/lib/platform/iplatformframe.h>

namespace cws80 {

NativeUI_Vst::NativeUI_Vst(VSTGUI::CFrame *parent)
    : parent_(parent) {
  realnat_.reset(NativeUI::create(
      parent->getPlatformFrame()->getPlatformRepresentation()));
}

std::string NativeUI_Vst::choose_file(
    gsl::span<const FileChooserFilter> filters, const std::string &directory,
    const std::string &title, FileChooserMode mode) {
  CFrame *parent = parent_;

  CNewFileSelector::Style style = (mode == FileChooserMode::Save) ?
      CNewFileSelector::kSelectSaveFile : CNewFileSelector::kSelectFile;

  CNewFileSelector *fs = CNewFileSelector::create(parent, style);
  if (!fs)
    throw std::runtime_error("cannot create file selector");
  SCOPE(exit) { fs->forget(); };

  if (!title.empty())
    fs->setTitle(UTF8String(title));
  if (!directory.empty())
    fs->setInitialDirectory(UTF8String(directory));

  for (const FileChooserFilter &filter : filters) {
    for (const char *pat : filter.patterns) {
      if (pat[0] == '*' && pat[1] == '.') {
        CFileExtension fext(filter.name, UTF8String(pat + 2));
        fs->addFileExtension(std::move(fext));
      }
    }
  }

  if (!fs->runModal())
    return {};
  return fs->getSelectedFile(0);
}

cxx::optional<std::string> NativeUI_Vst::edit_line(
    const std::string &title, const std::string &initial_value) {
  // not in VSTGUI, fall back to real native
  NativeUI &nat = *realnat_;
  return nat.edit_line(title, initial_value);
}

opt_uint NativeUI_Vst::select_by_menu(
    gsl::span<const std::string> choices, opt_uint initial_selection) {
  CFrame *parent = parent_;

  COptionMenu *menu = new COptionMenu();
  SCOPE(exit) { menu->forget(); };

  for (const std::string &choice : choices)
    menu->addEntry(UTF8String(choice));

  if (initial_selection)
    menu->setCurrent(*initial_selection);

  CPoint point {};
  parent->getPlatformFrame()->getCurrentMousePosition(point);

  if (!menu->popup(parent, point))
    return {};

  return menu->getCurrentIndex();
}

}  // namespace cws80
