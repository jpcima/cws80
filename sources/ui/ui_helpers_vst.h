#pragma once
#include "ui_helpers_native.h"
#include <memory>

namespace VSTGUI { class CFrame; }

//
namespace cws80 {

class NativeUI_Vst : public NativeUI {
 public:
  explicit NativeUI_Vst(VSTGUI::CFrame *parent);

  std::string choose_file(
      gsl::span<const FileChooserFilter> filters, const std::string &directory,
      const std::string &title, FileChooserMode mode) override;

  cxx::optional<std::string> edit_line(
      const std::string &title, const std::string &initial_value) override;

  opt_uint select_by_menu(
      gsl::span<const std::string> choices, opt_uint initial_selection) override;

 private:
  VSTGUI::CFrame *parent_ = nullptr;
  std::unique_ptr<NativeUI> realnat_;
};

}  // namespace cws80
