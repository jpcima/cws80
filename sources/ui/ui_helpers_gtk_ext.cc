#include "utility/scope_guard.h"
#include "utility/optional.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include <gtk/gtk.h>
#ifndef NO_DYNAMIC_API
# include "api/gtk3.h"
#endif
#include <getopt.h>
#include <fmt/format.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/next_prior.hpp>
#include <c++std/string_view>
#include <vector>
#include <system_error>

static void emit_message(cxx::string_view msg);

//------------------------------------------------------------------------------
static void dialog_accept(GtkDialog *dialog) {
  gtk_dialog_response(dialog, GTK_RESPONSE_ACCEPT);
}

static void get_mouse_position(int *x, int *y) {
  GdkDisplay *dpy = gdk_display_get_default();
  GdkSeat *seat = gdk_display_get_default_seat(dpy);
  GdkDevice *dev = gdk_seat_get_pointer(seat);
  gdk_device_get_position(dev, nullptr, x, y);
}

//------------------------------------------------------------------------------
static int cmd_choose_file(int argc, char *argv[]) {
  struct Filter {
    std::string name;
    std::vector<std::string> patterns;
  };

  cxx::string_view mode_str = "open";
  const char *directory = nullptr;
  const char *title = nullptr;
  std::vector<Filter> filters;

  while (opt_uint c = getopt(argc, argv, "m:d:t:f:")) {
    switch (*c) {
      case 'm': mode_str = optarg; break;
      case 'd': directory = optarg; break;
      case 't': title = optarg; break;
      case 'f': {
        std::vector<std::string> tokens;
        boost::split(tokens, optarg, [](char c) -> bool { return c == ','; });
        if (tokens.empty())
          throw std::logic_error("invalid filter argument");
        Filter flt;
        flt.name.assign(tokens[0]);
        flt.patterns.assign(boost::next(tokens.begin()), tokens.end());
        filters.push_back(std::move(flt));
        break;
      }
      default: throw std::logic_error("invalid argument");
    }
  }

  enum class Mode { Open, Save };
  Mode mode;

  if (mode_str == "open") mode = Mode::Open;
  else if (mode_str == "save") mode = Mode::Save;
  else throw std::logic_error("invalid file chooser mode");

  const char *buttonlabel = (mode == Mode::Open) ? "_Open" : "_Save";

  GtkWidget *dialog = gtk_file_chooser_dialog_new(
      title, nullptr,
      (GtkFileChooserAction)mode,
      "_Cancel", GTK_RESPONSE_CANCEL,
      buttonlabel, GTK_RESPONSE_ACCEPT, nullptr);
  SCOPE(exit) { gtk_widget_destroy(dialog); };

  for (const Filter &filter : filters) {
    GtkFileFilter *flt = gtk_file_filter_new();
    gtk_file_filter_set_name(flt, filter.name.c_str());
    for (const std::string &pat : filter.patterns)
      gtk_file_filter_add_pattern(flt, pat.c_str());
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), flt);
  }

  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), directory);

  int res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res != GTK_RESPONSE_ACCEPT)
    return {};

  char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
  SCOPE(exit) { g_free(filename); };

  if (filename)
    emit_message(filename);
  return 0;
}

//------------------------------------------------------------------------------
static int cmd_edit_line(int argc, char *argv[]) {
  const char *initvalue = nullptr;
  const char *title = nullptr;

  while (opt_uint c = getopt(argc, argv, "v:t:")) {
    switch (*c) {
      case 'v': initvalue = optarg; break;
      case 't': title = optarg; break;
      default: throw std::logic_error("invalid argument");
    }
  }

  GtkWidget *dialog = gtk_dialog_new_with_buttons(
      title, nullptr,
      GTK_DIALOG_MODAL,
      "_Cancel", GTK_RESPONSE_CANCEL,
      "_OK", GTK_RESPONSE_ACCEPT, nullptr);
  SCOPE(exit) { gtk_widget_destroy(dialog); };

  GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

  GtkWidget *entry = gtk_entry_new();
  gtk_container_add(GTK_CONTAINER(content_area), entry);

  g_signal_connect_swapped(
      entry, "activate", (GCallback)&dialog_accept, GTK_DIALOG(dialog));

  if (initvalue)
    gtk_entry_set_text(GTK_ENTRY(entry), initvalue);

  gtk_widget_show_all(dialog);

  int res = gtk_dialog_run(GTK_DIALOG(dialog));
  if (res != GTK_RESPONSE_ACCEPT)
    emit_message("-");
  else
    emit_message(fmt::format("+{}", gtk_entry_get_text(GTK_ENTRY(entry))));

  return 0;
}

//------------------------------------------------------------------------------
struct select_by_menu_context {
  GtkWidget *item;
  uint id;
  opt_uint *dst;
  bool *complete;
};

static void select_by_menu_cb_done(GtkMenuShell *menu, void *user_data) {
  select_by_menu_context *ctx = (select_by_menu_context *)user_data;
  *ctx->complete = true;
}

static void select_by_menu_cb_activate(GtkMenuItem *item, void *user_data) {
  select_by_menu_context *ctx = (select_by_menu_context *)user_data;
  ctx->dst->reset(ctx->id);
}

static int cmd_select_option(int argc, char *argv[]) {
  opt_uint initial_selection;

  while (opt_uint c = getopt(argc, argv, "v:")) {
    switch (*c) {
      case 'v': initial_selection = boost::lexical_cast<uint>(optarg); break;
      default: throw std::logic_error("invalid argument");
    }
  }

  uint nchoices = (uint)argc - optind;
  const char **choices = (const char **)argv + optind;

  if (nchoices == 0)
    return 0;

  if (initial_selection && *initial_selection >= nchoices)
    throw std::logic_error("invalid initial selection");

  GdkWindow *root = gdk_get_default_root_window();
  int x = 0, y = 0;
  get_mouse_position(&x, &y);

  bool complete = false;
  opt_uint selection;
  dynarray<select_by_menu_context> ctx(nchoices);

  GtkWidget *menu = gtk_menu_new();
  SCOPE(exit) { gtk_widget_destroy(menu); };

  for (uint i = 0; i < nchoices; ++i) {
    GtkWidget *item = gtk_menu_item_new_with_label(choices[i]);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
    ctx[i].item = item;
    ctx[i].id = i;
    ctx[i].dst = &selection;
    ctx[i].complete = &complete;
    g_signal_connect(
        item, "activate", G_CALLBACK(&select_by_menu_cb_activate), &ctx[i]);
  }

  g_signal_connect(
      menu, "selection-done", G_CALLBACK(&select_by_menu_cb_done), &ctx[0]);

  gtk_widget_show_all(menu);

  GdkRectangle rect {x, y, 0, 0};
  gtk_menu_popup_at_rect(
      GTK_MENU(menu),
      root, &rect, GDK_GRAVITY_WEST, GDK_GRAVITY_WEST, nullptr);

  if (initial_selection) {
    GtkWidget *item = ctx[*initial_selection].item;
    gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), item);
  }

  while (!complete && gtk_widget_is_visible(menu))
    gtk_main_iteration();

  if (selection)
    emit_message(std::to_string(*selection));
  return 0;
}

//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
  if (argc < 2)
    throw std::logic_error("too few arguments");

  cxx::string_view cmd = argv[1];
  --argc;
  ++argv;

#ifndef NO_DYNAMIC_API
  gtk3::load_api();
#endif

  gtk_init(nullptr, nullptr);

  if (cmd == "choose-file")
    return cmd_choose_file(argc, argv);
  else if (cmd == "edit-line")
    return cmd_edit_line(argc, argv);
  else if (cmd == "select-option")
    return cmd_select_option(argc, argv);

  throw std::logic_error("unknown subcommand");
}

//------------------------------------------------------------------------------
static void emit_message(cxx::string_view msg) {
  const char *data = msg.data();
  size_t nb = msg.size();
  while (nb) {
    opt_size_t count = write(STDOUT_FILENO, data, nb);
    if (!count) {
      if (errno == EINTR) continue;
      throw std::system_error(errno, std::generic_category());
    }
    nb -= *count;
    data += *count;
  }
}
