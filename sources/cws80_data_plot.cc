#include "cws80_data_plot.h"
#include "utility/gnuplot.h"
#include "utility/dynarray.h"
#include <memory>

namespace cws80 {

void plot_waves(const Wave waves[], const char *titles[], uint count) {
  StdioFile gp = gp_open();

  gp.puts("set terminal wxt size 1024,480\n");
  gp_xrange(gp, 0, 1);

  gp.puts("plot");
  for (uint i = 0; i < count; ++i) {
    gp.fmt("{} '-' using 1:2 title \"{}\" with lines",
           (i == 0) ? "" : ",", gp_escape(titles[i]));
  }
  gp.putc('\n');

  for (uint i = 0; i < count; ++i) {
    Sample sample = wave_sample(waves[i]);
    for (uint i = 0, n = sample.length(); i < n; ++i)
      gp.fmt("{} {}\n", i / (f64)(n - 1), (int)sample.data[i] - 128);
    gp.puts("e\n");
  }
  gp_pause(gp);
  gp.flush();
}

void plot_waveset(Waveset waveset) {
  Wave waves[16];
  dynarray<const char *> names(16);
  dynarray<char> namebuf(16 * 16);
  for (uint i = 0; i < 16; ++i) {
    uint id = waveset.wavenum[i];
    waves[i] = wave_by_id(id);
    names[i] = wave_name(id, &namebuf[i * 16]);
  }
  plot_waves(waves, names.data(), 16);
}

}  // namespace cws80
