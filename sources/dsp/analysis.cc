#include "dsp/analysis.h"
#include "dsp/fft.h"
#include "dsp/fir-exec.h"
#include "dsp/biquad-design.h"
#include "dsp/biquad-exec.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include <algorithm>
#include <memory>

namespace dsp {

void anatransfer(
    const f64 *in, const f64 *out, uint n, c64 *spec,
    Window wk, f64 wp) {
  uint specn = n / 2 + 1;

  FFT fft(n);

  dynarray<c64> specin(specn);
  dynarray<c64> specout(specn);

  { gsl::span<c64> spec = fft.forward(in, wk, wp);
    std::copy_n(spec.data(), specn, specin.data()); }
  { gsl::span<c64> spec = fft.forward(out, wk, wp);
    std::copy_n(spec.data(), specn, specout.data()); }

  for (uint i = 0; i < specn; ++i)
    spec[i] = specout[i] / specin[i];
}

void anafilter(
    const anafilter_run_t &fn, gsl::span<c64> spec,
    Window wk, f64 wp) {
  uint specn = spec.size();
  if (specn == 0)
    return;

  uint n = (specn - 1) * 2;

  // create signal from full spectrum
  FFT fft(n);
  std::fill_n(spec.data(), specn, 1);
  gsl::span<f64> in = fft.backward(spec.data());
  // normalize to 1
  f64 inmax = 0;
  for (f64 x : in)
    inmax = std::max(inmax, std::abs(x));
  for (f64 &x : in)
    x /= inmax;

  dynarray<f64> out(n);
  fn(in.data(), out.data(), n);
  anatransfer(in.data(), out.data(), n, spec.data(), wk, wp);
}

void anafir(
    gsl::span<const f64> h, gsl::span<c64> spec,
    Window wk, f64 wp) {
  uint hlen = h.size();
  Fir<f64> filter(hlen);
  anafilter([&](const f64 *in, f64 *out, uint n) {
      filter.run(in, out, n, h.data());
    }, spec, wk,  wp);
}

void anabq(
    const biquad_design &bqd, gsl::span<c64> spec,
    Window wk, f64 wp) {
  biquad<f64> filter;
  bqd.apply_to(filter);
  anafilter([&](const f64 *in, f64 *out, uint n) {
      filter.run(in, out, n);
    }, spec, wk,  wp);
}

void anabq(
    gsl::span<const biquad_design> bqd, gsl::span<c64> spec,
    Window wk, f64 wp) {
  if (bqd.empty())
    throw std::logic_error("empty filter cascade");
  uint nbq = bqd.size();
  dynarray<biquad<f64>> cascade(nbq);
  for (uint i = 0; i < nbq; ++i)
    bqd[i].apply_to(cascade[i]);
  anafilter([&](const f64 *in, f64 *out, uint n) {
      cascade[0].run(in, out, n);
      for (uint i = 1; i < nbq; ++i)
        cascade[i].run(out, out, n);
    }, spec, wk,  wp);
}

}  // namespace dsp
