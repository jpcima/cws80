#pragma once
#include "utility/types.h"
#include <type_traits>
#include <algorithm>
#include <memory>

namespace dsp {

template <class S>
struct Fir {
  static_assert(std::is_floating_point<S>());

  Fir() = default;
  explicit Fir(uint taps);

  void reset();
  void in(S x);
  S out(const S *coef) const;
  S tick(S x, const S *coef);
  void run(const f64 *inp, f64 *outp, uint n, const S *coef);

  uint i_ = 0, n_ = 0;
  std::unique_ptr<S[]> h_;
};

//------------------------------------------------------------------------------
template <class S>
Fir<S>::Fir(uint taps)
    : n_(taps),
      h_(new S[2 * taps]()) {
}

template <class S>
void Fir<S>::reset() {
  std::fill_n(h_.get(), 2 * n_, 0);
}

template <class S>
void Fir<S>::in(S x) {
  uint i = i_;
  uint n = n_;
  i = (i > 0) ? (i - 1) : (n - 1);
  h_[i] = x;
  h_[i + n] = x;
  i_ = i;
}

template <class S>
S Fir<S>::out(const S *coef) const {
  S sum = 0;
  uint i = i_;
  const uint n = n_;
  const S *__restrict h = h_.get();
#pragma omp simd reduction(+:sum)
  for (uint j = 0; j < n; ++j)
    sum += coef[j] * h[i + j];
  return (S)sum;
}

template <class S>
S Fir<S>::tick(S x, const S *coef) {
  in(x);
  return out(coef);
}

template <class S>
void Fir<S>::run(const f64 *inp, f64 *outp, uint n, const S *coef) {
  for (uint i = 0; i < n; ++i) {
    in(inp[i]);
    outp[i] = out(coef);
  }
}

}  // namespace dsp
