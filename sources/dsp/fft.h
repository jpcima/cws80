#pragma once
#include "dsp/window.h"
#include "utility/types.h"
#include <gsl/span>
#include <memory>

namespace dsp {

class FFT {
 public:
  explicit FFT(uint n);
  ~FFT();
  gsl::span<c64> forward(const f64 x[]);
  gsl::span<f64> backward(const c64 x[]);

  gsl::span<c64> forward(const f64 x[], Window wk, f64 wp);
  gsl::span<f64> backward(const c64 x[], Window wk, f64 wp);

 private:
  struct Impl;
  std::unique_ptr<Impl> P;
};

}  // namespace dsp
