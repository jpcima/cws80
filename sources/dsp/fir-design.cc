#include "dsp/fir-design.h"
#include "utility/dynarray.h"
#include <numeric>
#include <math.h>
#include <assert.h>

namespace dsp {

static void wsinc(gsl::span<f64> h, f64 c, Window wk, f64 wp) {
  uint n = h.size();
  window_function(wk)(h.data(), n, wp);
  f64 step = c * 2 * M_PI;
  f64 start = -step * (n - 1) / 2;
  for (uint i = 0; i < n; ++i) {
    f64 phase = start + step * i;
    h[i] *= (phase == 0) ? 1 : (sin(phase) / phase);
  }
}

static void unitynorm(gsl::span<f64> h) {
  f64 g = 1 / std::accumulate(h.begin(), h.end(), 0.0);
  for (f64 &x : h)
    x *= g;
}

static void spectral_inversion(gsl::span<f64> h) {
  uint n = h.size();
  assert(n & 1);
  for (unsigned i = 0; i < n; ++i)
    h[i] = -h[i];
  h[n / 2] += 1;
}

static void spectral_reversal(gsl::span<f64> h) {
  uint n = h.size();
  assert(n & 1);
  for (unsigned i = 0; i < n; i += 2)
    h[i] = -h[i];
}

void firwin(
    FirType t, gsl::span<f64> h, std::pair<f64, f64> c,
    Window wk, f64 wp) {
  uint nc = h.size();
  if (nc < 2)
    throw std::logic_error("invalid number of coefficients");

  if (t != FirType::LP && nc % 2 == 0)
    throw std::logic_error("cannot design even length filters other than LP");

  switch (t) {
    case FirType::LP:
    case FirType::HP:
      wsinc(h, c.first, wk, wp);
      unitynorm(h);
      if (t == FirType::HP)
        // convert low-pass to high-pass
        spectral_inversion(h);
      break;
    case FirType::BP:
    case FirType::BS:
      dynarray<f64> lp(nc);
      dynarray<f64> hp(nc);
      // design low-pass
      wsinc(lp, c.first, wk, wp);
      unitynorm(lp);
      // design high-pass
      wsinc(hp, c.second, wk, wp);
      unitynorm(hp);
      spectral_inversion(hp);
      // combine into band-stop
      for (unsigned i = 0; i < nc; ++i)
        h[i] = lp[i] + hp[i];
      if (t == FirType::BP)
        // convert band-stop to band-pass
        spectral_inversion(h);
      break;
  }
}

}  // namespace dsp
