#include "dsp/biquad-design.h"
#include <math.h>

namespace dsp {

void biquad_design::lp(f64 fc, f64 q) {
  f64 w = fc * 2 * M_PI;
  f64 cw = cos(w);
  f64 sw = sin(w);
  f64 alpha = sw / (2 * q);
  b0_ = (1 - cw) / 2;
  b1_ = 1 - cw;
  b2_ = (1 - cw) / 2;
  a0_ = 1 + alpha;
  a1_ = -2 * cw;
  a2_ = 1 - alpha;
}

void biquad_design::hp(f64 fc, f64 q) {
  f64 w = fc * 2 * M_PI;
  f64 cw = cos(w);
  f64 sw = sin(w);
  f64 alpha = sw / (2 * q);
  b0_ = (1 + cw) / 2;
  b1_ = - (1 + cw);
  b2_ = (1 + cw) / 2;
  a0_ = 1 + alpha;
  a1_ = - 2 * cw;
  a2_ = 1 - alpha;
}

}  // namespace dsp
