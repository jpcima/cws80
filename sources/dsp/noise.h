#pragma once
#include "utility/types.h"
#include <random>

namespace dsp {

class WhiteNoise {
 public:
  f64 tick() {
    std::uniform_real_distribution<f64> dist;
    return dist(rand_);
  }
 private:
  std::minstd_rand rand_;
};

}  // namespace dsp
