#pragma once
#include "dsp/biquad-exec.h"
#include "utility/types.h"

namespace dsp {

class biquad_design {
 public:
  void lp(f64 fc, f64 q);
  void hp(f64 fc, f64 q);

  template <class S> void apply_to(biquad<S> &bq) const;

 private:
  f64 b0_ = 0, b1_ = 0, b2_ = 0;
  f64 a0_ = 0, a1_ = 0, a2_ = 0;
};

//------------------------------------------------------------------------------
template <class S>
void biquad_design::apply_to(biquad<S> &bq) const {
  bq.coefs(b0_, b1_, b2_, a0_, a1_, a2_);
}

}  // namespace dsp
