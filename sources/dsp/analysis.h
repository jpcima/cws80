#pragma once
#include "dsp/window.h"
#include "utility/types.h"
#include <gsl/span>
#include <functional>

namespace dsp {

class biquad_design;

//
typedef std::function<void(const f64 *, f64 *, uint)> anafilter_run_t;

//
void anatransfer(
    const f64 *in, const f64 *out, uint n, c64 *spec,
    Window wk, f64 wp);

void anafilter(
    const anafilter_run_t &fn, gsl::span<c64> spec,
    Window wk, f64 wp);

void anafir(
    gsl::span<const f64> h, gsl::span<c64> spec,
    Window wk, f64 wp);

void anabq(
    const biquad_design &bqd, gsl::span<c64> spec,
    Window wk, f64 wp);

void anabq(
    gsl::span<const biquad_design> bqd, gsl::span<c64> spec,
    Window wk, f64 wp);

}  // namespace dsp
