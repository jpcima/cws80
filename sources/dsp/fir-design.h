#pragma once
#include "dsp/window.h"
#include "utility/types.h"
#include <gsl/span>
#include <utility>

namespace dsp {

enum class FirType {
  LP, HP, BP, BS,
};

void firwin(
    FirType t, gsl::span<f64> h, std::pair<f64, f64> c,
    Window wk, f64 wp);

}  // namespace dsp
