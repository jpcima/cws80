#pragma once
#include "utility/types.h"
#include <type_traits>

namespace dsp {

template <class S>
class biquad {
 public:
  S tick(S x);
  template <class In, class Out> void run(const In *inp, Out *outp, uint n);
  void coefs(S b0, S b1, S b2, S a1, S a2);
  void coefs(S b0, S b1, S b2, S a0, S a1, S a2);

 private:
  S b0_ = 0, b1_ = 0, b2_ = 0;
  S a1_ = 0, a2_ = 0;
  S m1_ = 0, m2_ = 0;
};

//------------------------------------------------------------------------------
template <class S>
inline S biquad<S>::tick(S x) {
  /* Transposed Direct Form II
   X -+> (*)b0 > (+) ----------+> Y
      |           ^            |
      |         [z-1] M1       |
      |           ^            |
      +> (*)b1 > (+) < (*)-a1 <+
      |           ^            |
      |         [z-1] M2       |
      |           ^            |
      +> (*)b2 > (+) < (*)-a2 <+
  */
  S y = m1_ + x * b0_;
  m1_ = m2_ + x * b1_ - y * a1_;
  m2_ = x * b2_ - y * a2_;
  return y;
}

template <class S>
template <class In, class Out>
inline void biquad<S>::run(const In *inp, Out *outp, uint n) {
  static_assert(std::is_floating_point<In>() && std::is_floating_point<Out>());
  for (uint i = 0; i < n; ++i)
    outp[i] = tick(inp[i]);
}

template <class S>
inline void biquad<S>::coefs(S b0, S b1, S b2, S a1, S a2) {
  b0_ = b0;
  b1_ = b1;
  b2_ = b2;
  a1_ = a1;
  a2_ = a2;
}

template <class S>
inline void biquad<S>::coefs(S b0, S b1, S b2, S a0, S a1, S a2) {
  b0_ = b0 / a0;
  b1_ = b1 / a0;
  b2_ = b2 / a0;
  a1_ = a1 / a0;
  a2_ = a2 / a0;
}

}  // namespace dsp
