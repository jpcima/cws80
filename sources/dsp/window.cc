#include "dsp/window.h"
#include "utility/arithmetic.h"
#include <boost/math/special_functions/bessel.hpp>
#include <array>
#include <math.h>
#include <string.h>

namespace dsp {

static constexpr std::array<window_function_type *, 6> functions {{
  &window_tukey, &window_kaiser, &window_hann, &window_blackman,
  &window_blackman_nutall, &window_gauss,
}};
static constexpr std::array<const char *, 6> names {{
  "Tukey", "Kaiser", "Hann", "Blackman",
  "Blackman-Nutall", "Gauss",
}};

//
window_function_type *window_function(Window wk) {
  if (uint(wk) >= functions.size())
    return nullptr;
  return functions[(uint)wk];
}

//
void window_tukey(f64 *w, uint n, f64 p) {
  for (uint i = 0; i < n; ++i) {
    const f64 x = (f64)i / (n - 1);
    if (x < p / 2)
      w[i] = (1 + cos(M_PI * (2 * x / p - 1))) / 2;
    else if (x > 1 - p / 2)
      w[i] = (1 + cos(M_PI * (2 * (x - 1) / p + 1))) / 2;
    else
      w[i] = 1;
  }
}

void window_kaiser(f64 *w, uint n, f64 p) {
  using boost::math::cyl_bessel_i;
  for (uint i = 0; i < n; ++i) {
    const f64 x = (f64)i / (n - 1);
    f64 arg = p * sqrt(1 - square(2 * x - 1));
    w[i] = cyl_bessel_i(0, arg) / cyl_bessel_i(0, p);
  }
}

void window_hann(f64 *w, uint n, f64) {
  for (uint i = 0; i < n; ++i) {
    const f64 r = f64(i) / (n - 1);
    w[i] = (1 - std::cos(2 * M_PI * r)) / 2;
  }
}

void window_blackman(f64 *w, uint n, f64) {
  for (uint i = 0; i < n; ++i) {
    const f64 a = 0.16;
    const f64 r = f64(i) / (n - 1);
    w[i] = (1 - a) / 2
           - cos(2 * M_PI * r) / 2
           + (a / 2) * cos(4 * M_PI * r);
  }
}

void window_blackman_nutall(f64 *w, uint n, f64) {
  const f64 a0 = 0.3635819;
  const f64 a1 = 0.4891775;
  const f64 a2 = 0.1365995;
  const f64 a3 = 0.0106411;
  for (uint i = 0; i < n; ++i) {
    f64 arg = 2 * M_PI * i / (n - 1);
    w[i] = a0 - a1 * cos(arg) + a2 * cos(2 * arg) - a3 * cos(3 * arg);
  }
}

void window_gauss(f64 *w, uint n, f64 p) {
  for (uint i = 0; i < n; ++i)
    w[i] = exp(-0.5 * square(
        (i - 0.5 * (n - 1)) / (p * 0.5 * (n - 1))));
}

Window window_of_name(cxx::string_view name) {
  uint i = 0;
  while (i < names.size() && name != names[i])
    ++i;
  return (Window)i;
}

const char *name_of_window(Window wk) {
  if ((uint)wk >= names.size())
    return nullptr;
  return names[(uint)wk];
}

}  // namespace dsp
