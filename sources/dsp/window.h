#pragma once
#include "c++std/string_view"
#include "utility/types.h"

namespace dsp {

enum class Window {
  Tukey, Kaiser, Hann, Blackman, BlackmanNutall, Gauss,
};

typedef void(window_function_type)(f64 *, uint, f64);
window_function_type *window_function(Window wk);

void window_tukey(f64 *w, uint n, f64 p);
void window_kaiser(f64 *w, uint n, f64 p);
void window_hann(f64 *w, uint n, f64 p);
void window_blackman(f64 *w, uint n, f64 p);
void window_blackman_nutall(f64 *w, uint n, f64 p);
void window_gauss(f64 *w, uint n, f64 p);

Window window_of_name(cxx::string_view name);
const char *name_of_window(Window wk);

}  // namespace dsp
