#include "dsp/fft.h"
#include "utility/dynarray.h"
#include <fftw3.h>
#ifndef NO_DYNAMIC_API
# include "api/fftw3.h"
#endif
#include <fmt/format.h>
#include <algorithm>
#include <unordered_map>
#include <memory>
#include <shared_mutex>

namespace dsp {

struct block_deleter {
  void operator()(void *p) { fftw_free(p); }
};
struct plan_deleter {
  void operator()(fftw_plan p) { fftw_destroy_plan(p); }
};

typedef std::unique_ptr<fftw_plan_s, plan_deleter> plan_ptr;

static std::shared_mutex planner_mutex;
static std::unordered_map<u64, plan_ptr> plan_cache;
static u64 get_plan_id(u32 size, int type, u32 flags);
static fftw_plan get_cached_plan(u64 id);
static fftw_plan create_cached_plan(
    u32 size, int type, u32 flags, f64 *real, c64 *cplx);

//------------------------------------------------------------------------------
struct FFT::Impl {
  uint n_;
  std::unique_ptr<f64[], block_deleter> real_;
  std::unique_ptr<c64[], block_deleter> cplx_;
  fftw_plan fplan_ = nullptr;
  fftw_plan bplan_ = nullptr;
};

//------------------------------------------------------------------------------
FFT::FFT(uint n)
    : P(new Impl) {
#ifndef NO_DYNAMIC_API
  fftw3::load_api();
#endif
  P->n_ = n;
  f64 *real = fftw_alloc_real(n);
  c64 *cplx = (c64 *)fftw_alloc_complex(n / 2 + 1);
  P->real_.reset(real);
  P->cplx_.reset(cplx);
  if (!real || !cplx)
    throw std::bad_alloc();
}

FFT::~FFT() {
}

gsl::span<c64> FFT::forward(const f64 x[]) {
  uint n = P->n_;
  f64 *real = P->real_.get();
  c64 *cplx = P->cplx_.get();
  fftw_plan plan = P->fplan_;
  if (!plan)
    plan = P->fplan_ = create_cached_plan(n, FFTW_FORWARD, 0, real, cplx);
  std::copy_n(x, n, real);
  fftw_execute_dft_r2c(plan, real, (fftw_complex *)cplx);
  return gsl::span<c64>((c64 *)cplx, n / 2 + 1);
}

gsl::span<f64> FFT::backward(const c64 x[]) {
  uint n = P->n_;
  f64 *real = P->real_.get();
  c64 *cplx = P->cplx_.get();
  fftw_plan plan = P->bplan_;
  if (!plan)
    plan = P->bplan_ = create_cached_plan(n, FFTW_BACKWARD, 0, real, cplx);
  std::copy_n(x, n / 2 + 1, cplx);
  fftw_execute_dft_c2r(plan, (fftw_complex *)cplx, real);
  return gsl::span<f64>(real, n);
}

gsl::span<c64> FFT::forward(const f64 x[], Window wk, f64 wp) {
  uint n = P->n_;
  dynarray<f64> win(n);
  window_function(wk)(win.data(), n, wp);
  for (uint i = 0; i < n; ++i)
    win[i] *= x[i];
  return forward(win.data());
}

//------------------------------------------------------------------------------
static u64 get_plan_id(u32 size, int type, u32 flags) {
  u64 id = 0;
  bool forward = type == FFTW_FORWARD;
  id |= forward ? 0 : 1;
  id |= (u64)flags << 1;
  id |= (u64)size << 32;
  return id;
}

static fftw_plan get_cached_plan(u64 id) {
  std::shared_lock<std::shared_mutex> planner_lock(planner_mutex);
  auto it = plan_cache.find(id);
  if (it == plan_cache.end())
    return nullptr;
  return it->second.get();
}

fftw_plan create_cached_plan(
    u32 size, int type, u32 flags, f64 *real, c64 *cplx) {
  u64 id = get_plan_id(size, type, flags);
  if (fftw_plan plan = get_cached_plan(id))
    return plan;

  std::unique_lock<std::shared_mutex> planner_lock(planner_mutex);
  plan_ptr &planref = plan_cache[id];
  if (planref)
    return planref.get();

  plan_ptr plan;
  switch (type) {
    case FFTW_FORWARD:
      plan.reset(fftw_plan_dft_r2c_1d(size, real, (fftw_complex *)cplx, flags));
      break;
    case FFTW_BACKWARD:
      plan.reset(fftw_plan_dft_c2r_1d(size, (fftw_complex *)cplx, real, flags));
      break;
  }
  if (!plan)
    throw std::bad_alloc();

  planref.reset(plan.get());
  return plan.release();
}

}  // namespace dsp
