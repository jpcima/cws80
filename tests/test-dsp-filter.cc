#include "dsp/fir-design.h"
#include "dsp/biquad-design.h"
#include "dsp/lpcfmoog.h"
#include "dsp/analysis.h"
#include "utility/gnuplot.h"
#include "utility/optional.h"
#include "utility/dynarray.h"
#include "utility/debug.h"
#include "utility/types.h"
#include <mutex>
#include <getopt.h>
#include <math.h>

using namespace dsp;

static void plotspec(gsl::span<const c64> spec) {
  uint specn = spec.size();

  StdioFile gp = gp_open();
  cxx::string_view t1 = "Magnitude (dB)";
  cxx::string_view t2 = "Phase (rad)";

  gp.puts("set terminal wxt size 1024,768\n");
  if (false)
    gp.fmt("set multiplot layout 2, 1\n");

  gp.fmt("set title \"{}\"\n", gp_escape(t1));
  gp.fmt("unset key\n");
  gp.fmt("set grid\n");
  gp_yrange(gp, -80, {});
  gp.fmt("plot '-' using 1:2 with lines\n");
  for (uint i = 0; i < specn; ++i) {
    f64 mag = 20 * log10(std::abs(spec[i]));
    gp.fmt("{:a} {:a}\n", 0.5 * i / (specn - 1), mag);
  }
  gp.puts("e\n");

  if (false) {
    gp.fmt("set title \"{}\"\n", gp_escape(t2));
    gp.fmt("unset key\n");
    gp.fmt("set grid\n");
    gp_yrange(gp, -M_PI, +M_PI);
    gp.fmt("plot '-' using 1:2 with lines\n");
    for (uint i = 0; i < specn; ++i) {
      f64 phase = std::arg(spec[i]);
      gp.fmt("{:a} {:a}\n", 0.5 * i / (specn - 1), phase);
    }
    gp.puts("e\n");
  }

  gp_pause(gp);
  gp.flush();
}

static void specfir(gsl::span<c64> spec) {
  static uint NC = 63;

  dynarray<f64> h(NC);
  firwin(
      FirType::LP, h, {0.2, 0.3},
      Window::Kaiser, 2 * M_PI);

  // for (uint i = 0; i < NC; ++i)
  //   fmt::print(" {:e}", h[i]);
  // fmt::print("\n");

  anafir(h, spec, Window::BlackmanNutall, 0);
}

static void specbq(gsl::span<c64> spec) {
  biquad_design bqd;
  bqd.lp(0.2, 4);
  anabq(bqd, spec, Window::BlackmanNutall, 0);
}

static void specbqcascade(gsl::span<c64> spec) {
  biquad_design bqd[2];
  f64 fc = 0.25;
  f64 q = sqrt(16.0);
  bqd[0].lp(fc, q);
  bqd[1].lp(fc, q);
  anabq(bqd, spec, Window::BlackmanNutall, 0);
}

static void speclpcfmoog(gsl::span<c64> spec) {
  lpcfmoog::nice_filter moog;
  moog.lp(0.2, 0.7);
  anafilter([&](const f64 *in, f64 *out, uint n) {
      for (uint i = 0; i < n; ++i)
        out[i] = moog.tick(in[i]);
    }, spec, Window::BlackmanNutall, 0);
}

static void process() {
  static uint N = 2 * 1024;

  //
  const uint specn = N / 2 + 1;
  dynarray<c64> spec(specn);

  // specfir(spec);
  // specbq(spec);
  // specbqcascade(spec);
  speclpcfmoog(spec);

  //
  plotspec(spec);
}

int main(int argc, char *argv[]) {
  while (opt_uint c = getopt(argc, argv, "h")) {
    switch (*c) {
      case 'h':
        fprintf(stderr, "test-dsp-filter\n");
        return 0;
      default: return 1;
    }
  }

  if (optind != argc)
    return 1;

  process();
  return 0;
}
