#include "dsp/window.h"
#include "utility/gnuplot.h"
#include "utility/optional.h"
#include "utility/debug.h"
#include "utility/types.h"
#include <getopt.h>
#include <fmt/format.h>
#include <boost/lexical_cast.hpp>
#include <memory>
#include <stdexcept>

using namespace dsp;

static uint N = 256;
static Window W = Window::Tukey;
static f64 P = 0.5;

static void process() {
  if (N == 0)
    throw std::logic_error("invalid size");

  window_function_type *wfn = window_function(W);
  if (!wfn)
    throw std::logic_error("no such window");

  std::unique_ptr<f64[]> wp(new f64[N]);
  wfn(wp.get(), N, P);

  StdioFile gp = gp_open();
  std::string title = fmt::format("{} window P={}", name_of_window(W), P);
  gp_xrange(gp, 0, (f64)(N - 1));
  gp.fmt("plot '-' using 1:2 title \"{}\"\n", gp_escape(title));
  for (uint i = 0; i < N; ++i)
    gp.fmt("{} {:a}\n", i, wp[i]);
  gp.puts("e\n");
  gp_pause(gp);
  gp.flush();
}

int main(int argc, char *argv[]) {
  while (opt_uint c = getopt(argc, argv, "hn:w:p:")) {
    switch (*c) {
      case 'h':
        fprintf(stderr, "test-dsp-window [-n points] [-w window] [-p parameter]\n");
        return 0;
      case 'n': N = boost::lexical_cast<uint>(optarg); break;
      case 'w': W = window_of_name(optarg); break;
      case 'p': P = boost::lexical_cast<f64>(optarg); break;
      default: return 1;
    }
  }

  if (optind != argc)
    return 1;

  process();
  return 0;
}
