#include "dsp/lpcfmoog.h"
#include "dsp/analysis.h"
#include "utility/arithmetic.h"
#include "utility/dynarray.h"
#include "utility/types.h"
#include <fmt/format.h>
#include <algorithm>
#include <mutex>
#include <stdio.h>

// compute the frequency of the peak of the response of the VCF at (f, q)
static f64 peakfreq(f64 f, f64 q, uint fftsize);
// compute the f parameter such that (f',q) cuts off at peak frequency fdst
static f64 cutoff_freq(f64 fdst, f64 q, f64 tol = 1e-4);
// compute the corrected frequency f', outputs it as tuples (f, q, f')
//  (f',q) is the VCF parameter which produces the response peaking at f
static void compute(FILE *file);

int main() {
  compute(stdout);
  return 0;
}

//------------------------------------------------------------------------------
static f64 peakfreq(f64 f, f64 q, uint fftsize) {
  uint fftnspec = fftsize / 2 + 1;

  dsp::lpcfmoog::nice_filter moog;
  moog.lp(f, q);

  dynarray<c64> spec(fftnspec);
  anafilter([&](const f64 *in, f64 *out, uint n) {
      for (uint i = 0; i < n; ++i)
        out[i] = moog.tick(in[i]);
    }, spec, dsp::Window::BlackmanNutall, 0);

  auto itmax = std::max_element(
      spec.begin(), spec.end(),
      [](c64 a, c64 b) -> bool { return std::abs(a) < std::abs(b); });

  uint index = itmax - spec.begin();
  f64 f2 = index * 0.5 / (fftnspec - 1);
  return f2;
}

static f64 cutoff_freq(f64 fdst, f64 q, f64 tol) {
  f64 step = 0.1;
  f64 fsrc = fdst;
  auto getdir = [&]() -> int {
    dsp::lpcfmoog::nice_filter moog;
    f64 fdstcurrent = peakfreq(fsrc, q, 64 * 1024);
    f64 distance = fdst - fdstcurrent;
    return (std::abs(distance) < tol) ? 0 : (distance > 0) ? +1 : -1;
  };
  int dir = getdir();
  if (dir == 0)
    return fsrc;
  fsrc += (dir > 0) ? +step : -step;
  int olddir = dir;
  while ((dir = getdir()) != 0) {
    if (dir != olddir)
      step *= 0.5;
    fsrc += (dir > 0) ? +step : -step;
    olddir = dir;
  }
  return fsrc;
}

static void compute(FILE *file) {
  const f64 qmin = 0.3;
  const f64 qmax = 0.7;
  const f64 fmin = 0.01;
  const f64 fmax = 0.25;
  const uint nsteps = 10;
  const f64 qdelta = (qmax - qmin) / nsteps;
  const f64 fdelta = (fmax - fmin) / nsteps;

  const uint nn = square(nsteps);
#pragma omp parallel for
  for (uint ij = 0; ij < nn; ++ij) {
    uint i = ij / nsteps;
    uint j = ij % nsteps;

    f64 q = qmin + i * qdelta;
    f64 f = fmin + j * fdelta;
    f64 f2 = cutoff_freq(f, q);

    static std::mutex mutex;
    std::lock_guard<std::mutex> lock(mutex);
    fmt::print(file, "{:e} {:e} {:e}\n", f, q, f2);
  }
}
